import {
    AwsCustomResource,
    AwsSdkCall,
    PhysicalResourceId,
} from 'aws-cdk-lib/custom-resources'
import { Construct } from 'constructs'
import * as iam from 'aws-cdk-lib/aws-iam'
import {Stack} from 'aws-cdk-lib'

/**
 * Represents the properties required to add an API stage to a usage plan.
 * @interface
 */
export interface AddApiStageToUsagePlanProps {
    UsagePlanId: string
    ApiId: string
    Stage: string
}

/**
 * Adds an API stage to a usage plan using an AWS Cloud Development Kit (CDK) construct.
 *
 * @param {Stack} stack - The AWS CDK stack to which the construct should be added.
 * @param {string} name - The name of the construct.
 * @param {AddApiStageToUsagePlanProps} props - The properties specifying the usage plan and API stage to be added.
 * @returns {Construct} The AWS CDK construct representing the operation to add an API stage to a usage plan.
 */
export const addApiStageToUsagePlan = (stack: Stack, name: string, props: AddApiStageToUsagePlanProps): Construct => {

    //implementation based on this ticket: https://github.com/aws/aws-cdk/discussions/22944#discussioncomment-6793333
    const usagePlanSdkCall = (op: string): AwsSdkCall => ({
        service: '@aws-sdk/client-api-gateway',
        action: 'UpdateUsagePlanCommand',
        parameters: {
            usagePlanId: props.UsagePlanId,
            patchOperations: [
                {
                    op,
                    path: '/apiStages',
                    value: `${props.ApiId}:${props.Stage}`,
                },
            ],
        },
        physicalResourceId: PhysicalResourceId.of(
            props.UsagePlanId + '--' + props.ApiId
        ),
    })

    return new AwsCustomResource(stack, name, {
        onCreate: usagePlanSdkCall('add'),
        onDelete: usagePlanSdkCall('remove'),
        installLatestAwsSdk: false,
        policy: {
            statements: [
                new iam.PolicyStatement({
                    resources: ['*'],
                    actions: ['apigateway:PATCH'],
                    effect: iam.Effect.ALLOW,
                }),
            ],
        },
    })
}

