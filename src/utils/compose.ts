export type ComposerFn<T> = (input: T) => T

export function compose<T>(initialValue: T, ...args: Array<ComposerFn<T>>): T {
    return args.reduce((acum, current) => {
        return current(acum)
    }, initialValue)
}