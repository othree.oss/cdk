import {Stack} from 'aws-cdk-lib'
import {CfnRoute, CfnVPCPeeringConnection, IVpc, SubnetSelection} from 'aws-cdk-lib/aws-ec2'

/**
 * @module vpc
 */

/**
 * Represents VPC peering configuration.
 * @interface
 */
export interface VpcPeering {
    readonly vpc: IVpc
    readonly subnetSelection: SubnetSelection
}

/**
 * Represents input configuration for setting up VPC peering connections.
 * @interface
 */
export interface VpcPeeringInput {
    readonly fromVpcPeering: VpcPeering
    readonly toVpcPeering: VpcPeering
}

/**
 * Creates a function that configures and establishes VPC peering connections.
 *
 * @param {Stack} stack - The AWS CloudFormation stack to which the resources will be added.
 * @returns A curried function that configures VPC peering connections.
 */
export const withVpcPeering = (stack: Stack) =>
    /**
     * Creates a function that configures and establishes VPC peering connections.
     *
     * @param {Function} withId - A function that generates identifiers for resources.
     * @returns A curried function that configures VPC peering connections.
     */
    (withId: (identifier: string) => string) =>
        /**
         * Configures and establishes VPC peering connections.
         *
         * @param {VpcPeeringInput} input - Input configuration for setting up VPC peering connections.
         * @returns {CfnVPCPeeringConnection} The configured VPC peering connection.
         */
        (input: VpcPeeringInput): CfnVPCPeeringConnection => {
            const peeringConnection = new CfnVPCPeeringConnection(stack, withId('PeeringConnection'), {
                vpcId: input.fromVpcPeering.vpc.vpcId,
                peerVpcId: input.toVpcPeering.vpc.vpcId
            })

            input.fromVpcPeering.vpc.selectSubnets(input.fromVpcPeering.subnetSelection).subnets
                .forEach(({routeTable: {routeTableId}}, index) => {
                    new CfnRoute(stack, withId(`PeeringRouteFromSubnet-${index}`), {
                        destinationCidrBlock: input.toVpcPeering.vpc.vpcCidrBlock,
                        routeTableId: routeTableId,
                        vpcPeeringConnectionId: peeringConnection.ref,
                    })
                })

            input.toVpcPeering.vpc.selectSubnets(input.toVpcPeering.subnetSelection).subnets
                .forEach(({routeTable: {routeTableId}}, index) => {
                    new CfnRoute(stack, withId(`PeeringRouteToSubnet-${index}`), {
                        destinationCidrBlock: input.fromVpcPeering.vpc.vpcCidrBlock,
                        routeTableId: routeTableId,
                        vpcPeeringConnectionId: peeringConnection.ref,
                    })
                })

            return peeringConnection
        }
