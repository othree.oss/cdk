import { Stack } from "aws-cdk-lib"
import {Role, ServicePrincipal} from 'aws-cdk-lib/aws-iam'
import {
    IMachineImage,
    Instance,
    ISecurityGroup,
    IVpc,
    Peer,
    Port,
    SecurityGroup,
    SubnetSelection,
    InstanceType
} from 'aws-cdk-lib/aws-ec2'
import {KeyPair} from 'cdk-ec2-key-pair'

/**
 * @module ec2
 */

/**
 * Creates a default EC2 instance role with the specified ID in the provided stack.
 *
 * @param {Stack} stack - The AWS CloudFormation stack where the role will be created.
 * @returns A function that takes the ID for the role and returns the created EC2 instance role.
 */
export const withDefaultEc2InstanceRole = (stack: Stack) =>
    /**
     * Creates a default EC2 instance role with the specified ID.
     *
     * @param {string} id - The ID for the EC2 instance role.
     * @returns {Role} - The created EC2 instance role.
     */
    (id: string): Role => {
        return new Role(stack, id, {
            assumedBy: new ServicePrincipal('ec2.amazonaws.com')
        })
    }

/**
 * Creates a default RDP (Remote Desktop Protocol) security group with the specified ID in the provided VPC.
 *
 * @param {Stack} stack - The AWS CloudFormation stack where the security group will be created.
 * @returns A function that takes the ID for the security group and the VPC and returns the created RDP security group.
 */
export const withDefaultRdpSecurityGroup = (stack: Stack) =>
    /**
     * Creates a default RDP security group with the specified ID in the provided VPC.
     *
     * @param {string} id - The ID for the RDP security group.
     * @param {IVpc} vpc - The VPC where the security group will be associated.
     * @returns {SecurityGroup} - The created RDP security group.
     */
    (id: string, vpc: IVpc): SecurityGroup => {
        const securityGroup = new SecurityGroup(stack, id, {
            vpc: vpc,
            allowAllOutbound: true
        })

        securityGroup.addIngressRule(Peer.anyIpv4(), Port.tcp(3389))

        return securityGroup
    }

/**
 * Creates a KeyPair resource with the specified name in the given AWS CloudFormation stack.
 *
 * @param {Stack} stack - The AWS CloudFormation stack where the KeyPair resource will be created.
 * @returns A function that takes the name for the KeyPair and returns the created KeyPair resource.
 */
export const withKeyPair = (stack: Stack) =>
    /**
     * Creates a KeyPair resource with the specified name.
     *
     * @param {string} name - The name for the KeyPair.
     * @returns {KeyPair} - The created KeyPair resource.
     */
    (name: string): KeyPair => {
        return new KeyPair(stack, name, {
            name: name,
            storePublicKey: true
        })
    }

/**
 * Represents the input configuration for creating an EC2 instance.
 */
export interface Ec2InstanceInput {
    readonly name: string
    readonly image: IMachineImage
    readonly vpc: IVpc
    readonly subnetSelection: SubnetSelection
    readonly instanceType: InstanceType
    readonly securityGroup: ISecurityGroup
}

/**
 * Creates an EC2 instance resource in the specified AWS CloudFormation stack.
 *
 * @param {Stack} stack - The AWS CloudFormation stack where the EC2 instance resource will be created.
 * @returns A function chain that takes various dependencies and input parameters to create an EC2 instance.
 */
export const withEc2Instance = (stack: Stack) =>
    /**
     * Specifies an identifier transformation function for naming resources.
     *
     * @param {Function} withId - A function that transforms an identifier.
     * @returns A function chain that takes various dependencies and input parameters to create an EC2 instance.
     */
    (withId: (identifier: string) => string) =>
        /**
         * Specifies a function to create an IAM role for the EC2 instance.
         *
         * @param {Function} withRole - A function that creates an IAM role.
         * @returns A function chain that takes various dependencies and input parameters to create an EC2 instance.
         */
        (withRole: (id: string) => Role) =>
            /**
             * Specifies a function to create a KeyPair for the EC2 instance.
             *
             * @param {Function} withKeyPair - A function that creates a KeyPair.
             * @returns A function chain that takes input parameters to create an EC2 instance.
             */
            (withKeyPair: (name: string) => KeyPair) =>
                /**
                 * Creates an EC2 instance based on the provided input configuration.
                 *
                 * @param {Ec2InstanceInput} input - The input configuration for creating the EC2 instance.
                 * @returns {Instance} - The created EC2 instance.
                 */
                (input: Ec2InstanceInput): Instance => {
                    const keyPair = withKeyPair(withId(`${input.name}-KeyPair`))
                    const instance = new Instance(stack, withId(input.name), {
                        instanceName: withId(input.name),
                        vpc: input.vpc,
                        vpcSubnets: input.vpc.selectSubnets(input.subnetSelection),
                        instanceType: input.instanceType,
                        machineImage: input.image,
                        securityGroup: input.securityGroup,
                        role: withRole(withId(`${input.name}-Role`)),
                        keyName: keyPair.keyPairName
                    })

                    return instance
                }
