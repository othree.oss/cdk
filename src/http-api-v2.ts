import {compose, ComposerFn} from './utils/compose'
import {Optional} from '@othree.io/optional'
import {RemovalPolicy, Stack} from 'aws-cdk-lib'
import {
    AddRoutesOptions,
    ApiMapping,
    CorsHttpMethod,
    DomainName,
    HttpApi,
    HttpApiProps,
    HttpMethod, HttpRoute, HttpRouteKey,
    IHttpApi, IHttpRoute, IHttpRouteAuthorizer,
} from '@aws-cdk/aws-apigatewayv2-alpha'
import {HttpJwtAuthorizer} from '@aws-cdk/aws-apigatewayv2-authorizers-alpha'
import {LogGroup} from 'aws-cdk-lib/aws-logs'
import {Cors} from 'aws-cdk-lib/aws-apigateway'
import {Effect, ManagedPolicy, PolicyStatement, Role, ServicePrincipal} from 'aws-cdk-lib/aws-iam'
import {CfnStage} from 'aws-cdk-lib/aws-apigatewayv2'
import {IFunction} from 'aws-cdk-lib/aws-lambda'
import {HttpLambdaIntegration} from '@aws-cdk/aws-apigatewayv2-integrations-alpha'

/**
 * @module httpApi
 */

// Default stage name
const DEFAULT_STAGE = 'release'

/**
 * Represents a custom domain for an HTTP API.
 */
export interface HttpApiDomain {
    readonly domain: string
    readonly basePath: string
}

/**
 * Represents the input configuration for creating an HTTP API.
 */
export interface HttpApiInput {
    readonly apiName: string
}

/**
 * Represents the configuration options for an HTTP API.
 */
export interface HttpApiConfiguration extends Partial<HttpApiProps> {
    readonly domain?: HttpApiDomain
}

/**
 * Specifies the default configuration for an HTTP API.
 *
 * @returns {HttpApiConfiguration} - The default configuration for an HTTP API.
 */
export const withDefaultApiConfiguration = (): HttpApiConfiguration => {
    return {
        corsPreflight: {
            allowCredentials: false,
            allowMethods: [CorsHttpMethod.GET],
            allowOrigins: Cors.ALL_ORIGINS,
            allowHeaders: Cors.DEFAULT_HEADERS
        }
    }
}

/**
 * Creates an HTTP API configuration with a JWT authorizer.
 *
 * @param {string} name - The name of the authorizer.
 * @param {string} issuer - The issuer of the JWT tokens.
 * @param {Array<string>} audience - The expected audience of the JWT tokens.
 * @returns {HttpApiConfiguration} - The HTTP API configuration with the JWT authorizer.
 */
export const withAuthorizerConfiguration = (name: string, issuer: string, audience: Array<string>): HttpApiConfiguration => {
    return {
        defaultAuthorizer: new HttpJwtAuthorizer(name, issuer, {
            authorizerName: name,
            jwtAudience: audience
        })
    }
}

/**
 * Creates an HTTP API and associated resources.
 *
 * @param {HttpApiConfiguration} defaultConfiguration - The default configuration for the HTTP API.
 * @returns A function to configure the HTTP API further.
 */
export const withHttpApi = (defaultConfiguration: HttpApiConfiguration) =>
    /**
     * Configures and creates the HTTP API.
     *
     * @param {Stack} stack - The CloudFormation stack.
     * @returns A function to provide input and additional configuration for the HTTP API.
     */
    (stack: Stack) =>
        /**
         * Configures and creates the HTTP API with the provided input and configuration.
         *
         * @param {HttpApiInput} input - The input configuration for the HTTP API.
         * @param {HttpApiProps} configuration - Additional configuration for the HTTP API.
         * @returns {IHttpApi} - The created HTTP API.
         */
        (input: HttpApiInput, configuration?: Partial<HttpApiProps>): IHttpApi => {
            const httpApi = new HttpApi(stack, input.apiName, {
                apiName: input.apiName,
                createDefaultStage: false,
                ...defaultConfiguration,
                ...Optional(configuration!).orElse({})
            })

            const logGroup = new LogGroup(stack, `${input.apiName}-LogGroup`, {
                logGroupName: `${httpApi.httpApiName}-${DEFAULT_STAGE}`,
                removalPolicy: RemovalPolicy.DESTROY
            })

            const role = new Role(stack, `${input.apiName}-Role`, {
                assumedBy: new ServicePrincipal('apigateway.amazonaws.com'),
            })

            const policy = new ManagedPolicy(stack, `${input.apiName}-Policy`, {
                statements: [
                    new PolicyStatement({
                        effect: Effect.ALLOW,
                        actions: [
                            'logs:CreateLogGroup',
                            'logs:CreateLogStream',
                            'logs:PutLogEvents'
                        ],
                        resources: ['*']
                    })
                ],
                roles: [role]
            })

            const cfnStage = new CfnStage(stack, `${input.apiName}-Stage`, {
                apiId: httpApi.apiId,
                accessPolicyId: policy.managedPolicyArn,
                stageName: DEFAULT_STAGE,
                autoDeploy: true,
                defaultRouteSettings: {
                    detailedMetricsEnabled: true,
                    loggingLevel: 'info'
                },
                accessLogSettings: {
                    destinationArn: logGroup.logGroupArn,
                    format: JSON.stringify({
                        requestId: '$context.requestId',
                        ip: '$context.identity.sourceIp',
                        requestTime: '$context.requestTime',
                        httpMethod: '$context.httpMethod',
                        routeKey: '$context.routeKey',
                        status: '$context.status',
                        protocol: '$context.protocol',
                        responseLength: '$context.responseLength',
                        userAgent: '$context.identity.userAgent',
                        integrationLatency: '$context.integrationLatency',
                        sub: '$context.authorizer.claims.sub',
                        responseLatency: '$context.responseLatency'
                    })
                }
            })

            Optional(defaultConfiguration.domain!).map(domainConfig => {
                const domain = DomainName.fromDomainNameAttributes(stack, `${input.apiName}-Domain`, {
                    name: domainConfig.domain
                } as any)

                new ApiMapping(stack, `${domain.name}-${domainConfig.basePath}`, {
                    api: httpApi,
                    domainName: domain,
                    stage: cfnStage as any,
                    apiMappingKey: domainConfig.basePath,
                })
            })

            return httpApi
        }

/**
 * Represents the configuration for an HTTP route.
 */
export interface HttpRouteConfiguration {
    readonly method: HttpMethod
    readonly fn: IFunction
    readonly options?: Partial<AddRoutesOptions>
}

/**
 * Adds a new HTTP route to the provided API.
 *
 * @param {string} path - The path of the route.
 * @param {HttpRouteConfiguration} configuration - The configuration of the route.
 * @returns A function that takes a ComposedHttpApi and returns an updated instance.
 */
export const withRoute = (path: string, configuration: HttpRouteConfiguration) =>
    /**
     * Adds a new HTTP route to the provided API.
     *
     * @param {ComposedHttpApi} api - The composed HTTP API instance.
     * @returns {ComposedHttpApi} - The updated composed HTTP API instance with the new route.
     */
    (api: ComposedHttpApi): ComposedHttpApi => {
        const lambdaIntegration = new HttpLambdaIntegration(`${path}-Fn`, configuration.fn)

        const httpRoute = new HttpRoute(api.api.stack, `${configuration.method}${path}`, {
            httpApi: api.api,
            routeKey: HttpRouteKey.with(path, configuration.method),
            integration: lambdaIntegration,
            authorizer: api.defaultHttpOptions.authorizer,
            ...Optional(configuration.options).orElse({})
        })

        const route = {
            [path]: httpRoute
        }

        return {
            routes: {
                ...api.routes,
                ...route
            },
            defaultHttpOptions: api.defaultHttpOptions,
            api: api.api
        }
    }

/**
 * Represents the default options for an HTTP API.
 */
export interface DefaultHttpApiOptions {
    readonly authorizer?: IHttpRouteAuthorizer
}

/**
 * Represents a composed HTTP API instance with routes and default options.
 */
export interface ComposedHttpApi {
    readonly api: IHttpApi
    readonly defaultHttpOptions: DefaultHttpApiOptions
    readonly routes: {
        [key: string]: IHttpRoute
    }
}

/**
 * Creates a new composed HTTP API instance with the specified default options.
 *
 * @param {DefaultHttpApiOptions} defaultOptions - The default options for the HTTP API.
 * @returns A function that takes an IHttpApi and a list of composer functions, returning a ComposedHttpApi instance.
 */
export const composedHttpApi = (defaultOptions: DefaultHttpApiOptions) =>
    /**
     * Composes an IHttpApi instance with additional functionality.
     *
     * @param {IHttpApi} api - The IHttpApi instance to compose.
     * @param {...Array<ComposerFn<ComposedHttpApi>>} args - List of composer functions to apply to the composed HTTP API.
     * @returns {ComposedHttpApi} - A composed HTTP API instance with the provided routes, default options, and additional functionality.
     */
    (api: IHttpApi, ...args: Array<ComposerFn<ComposedHttpApi>>): ComposedHttpApi => {
        const composedRestApi: ComposedHttpApi = {
            api: api,
            defaultHttpOptions: defaultOptions,
            routes: {}
        }

        return compose(composedRestApi, ...args)
    }
