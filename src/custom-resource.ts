import {Optional} from '@othree.io/optional'
import {marshall} from '@aws-sdk/util-dynamodb'
import {
    AwsCustomResource,
    AwsCustomResourcePolicy,
    AwsCustomResourceProps,
    AwsSdkCall,
    PhysicalResourceId,
} from 'aws-cdk-lib/custom-resources'
import {PolicyStatement, Role, ServicePrincipal} from 'aws-cdk-lib/aws-iam'
import {Duration, Stack} from 'aws-cdk-lib'

/**
 * @module customResource
 */

/**
 * Represents input for an AWS Lambda SDK call.
 */
export interface LambdaSdkCallInput {
    readonly payload: any
    readonly functionArn: string
    readonly hash: string
}

/**
 * Represents input for an AWS DynamoDB write operation.
 */
export interface DynamoWriteCallInput {
    readonly tableName: string
    readonly tableArn: string
    readonly name: string
    readonly items: Array<any>
}

/**
 * Represents the output of an AWS SDK call along with associated policy statement.
 */
export interface AwsSdkCallOutput {
    readonly awsSdkCall: AwsSdkCall
    readonly policyStatement: PolicyStatement

}

/**
 * Creates an AWS Lambda SDK call output and associated policy statement.
 *
 * @param input - The Lambda SDK call input containing payload, function ARN, and hash.
 * @returns An `AwsSdkCallOutput` object containing the AWS SDK call and policy statement.
 */
export const withLambdaSdkCall = (input: LambdaSdkCallInput): AwsSdkCallOutput => {
    const lambdaSdkCall: AwsSdkCall = {
        service: 'Lambda',
        action: 'invoke',
        parameters: {
            FunctionName: input.functionArn,
            Payload: JSON.stringify({
                ...input.payload,
                IgnoredHash: input.hash
            })
        },
        physicalResourceId: PhysicalResourceId.of(input.functionArn)
    }

    const policyStatement = new PolicyStatement({
        resources: [input.functionArn],
        actions: ['lambda:InvokeFunction']
    })

    return {
        awsSdkCall: lambdaSdkCall,
        policyStatement: policyStatement
    }
}

/**
 * Builds an array of DynamoDB PutRequest objects from an array of items.
 *
 * @param items - An array of items to be put into DynamoDB.
 * @returns An array of PutRequest objects with marshalled item data.
 */
const buildDynamoPutItems = (items: Array<any>) => {
    return items.map( item => {
        return {
            PutRequest: {
                Item: marshall(item)
            }
        }
    })
}

/**
 * Creates an AWS SDK call and corresponding IAM policy statement for DynamoDB batch write operation.
 *
 * @param input - DynamoWriteCallInput containing details of the batch write operation.
 * @returns AwsSdkCallOutput with the AWS SDK call and associated IAM policy statement.
 * @throws Error if the number of items in input.items is greater than 25.
 */
export const withDynamoBatchWriteCall = (input: DynamoWriteCallInput): AwsSdkCallOutput => {

    if(input.items.length > 25){
        throw new Error('Batch write items length should be 25 or less')
    }

    const dynamoBatchWriteSdkCall = {
        service: 'DynamoDB',
        action: 'batchWriteItem',
        parameters: {
            RequestItems: {
                [input.tableName]: buildDynamoPutItems(input.items)
            },
        },
        physicalResourceId: PhysicalResourceId.of(input.name)
    }

    const policyStatement = new PolicyStatement({
        resources: [input.tableArn],
        actions: ['dynamodb:BatchWriteItem']
    })

    return {
        awsSdkCall: dynamoBatchWriteSdkCall,
        policyStatement: policyStatement
    }
}

/**
 * Configuration for a custom resource with additional customization options.
 */
export interface CustomResourceConfiguration extends Partial<AwsCustomResourceProps> {
    readonly policy: AwsCustomResourcePolicy
}

/**
 * Creates a default CustomResourceConfiguration with commonly used settings.
 *
 * @returns Default CustomResourceConfiguration with a default policy and timeout.
 */
export const withDefaultCustomResourceConfiguration = (): CustomResourceConfiguration => {
    return {
        policy: AwsCustomResourcePolicy.fromSdkCalls({resources: AwsCustomResourcePolicy.ANY_RESOURCE}),
        timeout: Duration.minutes(10)
    }
}

/**
 * Input for creating a custom resource.
 */
export interface CustomResourceInput {
    readonly name: string
}

/**
 * Creates a custom resource with specified configurations and lifecycle hooks.
 *
 * @param stack The CDK stack to add the custom resource to.
 * @returns A function that takes input and additional configuration to create the custom resource.
 */
export const withCustomResource = (stack: Stack) =>
    /**
     * Creates a custom resource with specified configurations and lifecycle hooks.
     *
     * @param {Function} withId - A function to generate IDs.
     * @returns  A function that takes input and additional configuration to create the custom resource.
     */
    (withId: (identifier: string) => string) =>
        /**
         * Creates a custom resource with specified configurations and lifecycle hooks.
         *
         * @param {CustomResourceConfiguration} customResourceConfiguration - The default custom resource configuration.
         * @returns  A function that takes input and additional configuration to create the custom resource.
         */
        (customResourceConfiguration: CustomResourceConfiguration) =>
            /**
             * Creates a custom resource with specified configurations and lifecycle hooks.
             *
             * @param {AwsSdkCallOutput} [onCreateSdkCall] - The sdk call to execute when the custom resource is created.
             * @returns  A function that takes input and additional configuration to create the custom resource.
             */
            (onCreateSdkCall?: AwsSdkCallOutput) =>
                /**
                 * Creates a custom resource with specified configurations and lifecycle hooks.
                 *
                 * @param {AwsSdkCallOutput} [onUpdateSdkCall] - The sdk call to execute when the custom resource is updated.
                 * @returns  A function that takes input and additional configuration to create the custom resource.
                 */
                (onUpdateSdkCall?: AwsSdkCallOutput) =>
                    /**
                     * Creates a custom resource with specified configurations and lifecycle hooks.
                     *
                     * @param {AwsSdkCallOutput} [onDeleteSdkCall] - The sdk call to execute when the custom resource is deleted.
                     * @returns  A function that takes input and additional configuration to create the custom resource.
                     */
                    (onDeleteSdkCall?: AwsSdkCallOutput) =>
                        /**
                         * Creates a custom resource with specified configurations and lifecycle hooks.
                         *
                         * @param {CustomResourceInput} input - The custom resource details.
                         * @param {Partial<AwsCustomResourceProps>} [configuration] - The optional configuration that partial overrides the default one.
                         * @returns  {AwsCustomResource} The custom resource
                         */
                        (input: CustomResourceInput, configuration?: Partial<AwsCustomResourceProps>): AwsCustomResource => {
                            const awsCustomResourceRole = new Role(stack,
                                withId(`${input.name}-Role`),
                                {
                                    assumedBy: new ServicePrincipal('lambda.amazonaws.com')
                                }
                            )

                            Optional(onCreateSdkCall).map(_ => awsCustomResourceRole.addToPolicy(_.policyStatement))
                            Optional(onUpdateSdkCall).map(_ => awsCustomResourceRole.addToPolicy(_.policyStatement))
                            Optional(onDeleteSdkCall).map(_ => awsCustomResourceRole.addToPolicy(_.policyStatement))

                            const initializers: Partial<AwsCustomResourceProps> = {
                                ...Optional(onCreateSdkCall).map(_ => ({
                                    onCreate: onCreateSdkCall!.awsSdkCall
                                })).orElse(undefined!),
                                ...Optional(onUpdateSdkCall).map(_ => ({
                                    onUpdate: onUpdateSdkCall!.awsSdkCall
                                })).orElse(undefined!),
                                ...Optional(onDeleteSdkCall).map(_ => ({
                                    onDelete: onDeleteSdkCall!.awsSdkCall
                                })).orElse(undefined!)
                            }

                            const awsCustomResource = new AwsCustomResource(stack, withId(`${input.name}`), {
                                ...customResourceConfiguration,
                                ...initializers,
                                role: awsCustomResourceRole,
                                ...Optional(configuration).orElse({}),
                                functionName: withId(`${input.name}-Handler`)
                            })

                            return awsCustomResource
                        }
