import {AppContext} from './core'
import {Optional} from '@othree.io/optional'
import {Attribute, AttributeType, BillingMode, ITable, Table, TableProps} from 'aws-cdk-lib/aws-dynamodb'
import {RemovalPolicy, Stack} from 'aws-cdk-lib'

/**
 * @module dynamo
 */

/**
 * Represents the input configuration for creating a DynamoDB table.
 */
export interface TableInput {
    readonly id: string
    readonly partitionKey: Attribute
    readonly sortKey?: Attribute
}

/**
 * Generates a TableInput object with the specified attributes for Chisel events.
 *
 * @param {string} id - The unique identifier for the table.
 * @returns {TableInput} - The generated TableInput object.
 */
export const withChiselEventsTableInput = (id: string): TableInput => {
    return {
        id: id,
        partitionKey: {name: 'contextId', type: AttributeType.STRING},
        sortKey: {name: 'eventDate', type: AttributeType.NUMBER},
    }
}

/**
 * Generates default configuration options for a DynamoDB table based on the provided AppContext.
 *
 * @param {AppContext} context - The AppContext to determine configuration options from.
 * @returns {Partial<TableProps>} - The generated default configuration options as a partial TableProps object.
 */
export const withDefaultConfiguration = (context: AppContext): Partial<TableProps> => {
    return {
        billingMode: BillingMode.PAY_PER_REQUEST,
        removalPolicy: context.volatile ? RemovalPolicy.DESTROY : RemovalPolicy.RETAIN,
    }
}


/**
 * Creates a DynamoDB table with the provided configuration options.
 *
 * @param {Partial<TableProps>} defaultConfiguration - The default configuration options for the table.
 * @returns A curried function that takes the stack, table input, and optional additional configuration to create the DynamoDB table.
 */
export const withTable = (defaultConfiguration: Partial<TableProps>) =>
    /**
     * Creates a DynamoDB table based on the provided input and configuration.
     *
     * @param {Stack} stack - The AWS CloudFormation stack where the table will be created.
     * @returns A curried function that takes the stack, table input, and optional additional configuration to create the DynamoDB table.
     */
    (stack: Stack) =>
        /**
         * Creates a DynamoDB table based on the provided input and configuration.
         *
         * @param {TableInput} input - The input configuration for the table.
         * @param {Partial<TableProps>} configuration - Additional configuration options for the table.
         * @returns {ITable} - The created DynamoDB table.
         */
        (input: TableInput, configuration?: Partial<TableProps>): ITable => {
            const table = new Table(stack, input.id, {
                ...defaultConfiguration,
                tableName: input.id,
                partitionKey: input.partitionKey,
                sortKey: input.sortKey,
                ...Optional(configuration).orElse({}),
            })

            return table
        }
