import {App, Stack, Tags, StackProps} from 'aws-cdk-lib'
import {Optional} from '@othree.io/optional'
import {IConstruct} from 'constructs'

/**
 * @module stack
 */

/**
 * Represents input configuration for an AWS CloudFormation stack.
 * @interface
 */
export interface StackInput {
    readonly id: string
}

/**
 * Represents a key-value pair for a tag used.
 * @interface
 */
export interface StackTag {
    readonly key: string
    readonly value: string
}

/**
 * Represents configuration settings for AWS CloudFormation stack tags.
 * @interface
 */
export interface StackConfiguration {
    readonly tags: Array<StackTag>
}

/**
 * Creates a function that adds a tag to a construct within a given scope.
 *
 * @param {IConstruct} scope - The scope of the construct to which the tag will be added.
 * @returns  A function that returns the added tag.
 */
export const withTag = (scope: IConstruct) =>
    /**
     * Adds a tag to the specified construct within the given scope.
     *
     * @param {StackTag} tag - The tag to be added.
     * @returns {StackTag} The added tag.
     */
    (tag: StackTag): StackTag => {
        Tags.of(scope).add(tag.key, tag.value)
        return tag
    }

/**
 * Represents metadata information for a project.
 * @interface
 */
export interface ProjectMetadata {
    readonly project: string
    readonly environment: string
    readonly owner: string
}

/**
 * Loads project metadata from the provided App instance's context.
 *
 * @param {App} app - The App instance from which to load the metadata.
 * @returns {ProjectMetadata} The loaded project metadata.
 */
export const loadProjectMetadata = (app: App): ProjectMetadata => {
    const metadata = app.node.tryGetContext('metadata')
    return {
        project: metadata.project,
        environment: app.node.tryGetContext('env'),
        owner: metadata.owner,
    }
}

/**
 * Creates a function that returns default configuration settings for AWS CloudFormation stack tags.
 *
 * @param {ProjectMetadata} projectMetadata - The metadata information for the project.
 * @param {string} applicationId - The identifier for the application.
 * @returns {StackConfiguration} Default configuration settings for AWS CloudFormation stack tags.
 */
export const withDefaultConfiguration = (projectMetadata: ProjectMetadata, applicationId: string): StackConfiguration => {
    return {
        tags: [
            {
                key: 'Project',
                value: projectMetadata.project,
            },
            {
                key: 'ApplicationId',
                value: applicationId,
            },
            {
                key: 'Environment',
                value: projectMetadata.environment,
            },
            {
                key: 'Owner',
                value: projectMetadata.owner,
            },
            {
                key: 'Version',
                value: Optional(process.env.CI_COMMIT_REF_NAME).orElse(`local-${process.env.USER}`),
            },
        ],
    }
}

/**
 * Creates a function that configures and returns an AWS CloudFormation stack instance.
 *
 * @param {StackConfiguration} defaultConfiguration - Default configuration settings for stack tags.
 * @returns A curried function returns an AWS CloudFormation stack instance.
 */
export const withStack = (defaultConfiguration: StackConfiguration) =>
    /**
     * Creates a function that configures and returns an AWS CloudFormation stack instance.
     *
     * @param {App} app - The App instance to which the stack will be added.
     * @returns A curried function returns an AWS CloudFormation stack instance.
     */
    (app: App) =>
        /**
         * Configures and returns an AWS CloudFormation stack instance.
         *
         * @param {StackInput} input - Input settings for the AWS CloudFormation stack.
         * @param {StackConfiguration} [configuration] - Optional configuration settings for stack tags.
         * @param {StackProps} [stackProperties] - Optional properties for the stack.
         * @returns {Stack} The configured AWS CloudFormation stack instance.
         */
        (input: StackInput, configuration?: StackConfiguration, stackProperties?: StackProps): Stack => {
            const stack = new Stack(app, input.id, stackProperties)
            const tagStack = withTag(stack)
            defaultConfiguration.tags.forEach(tag => tagStack(tag))
            Optional(configuration).map(configuration => configuration!.tags.forEach(tag => tagStack(tag)))

            return stack
        }
