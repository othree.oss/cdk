import {Queue, DeadLetterQueue, QueueProps, IQueue} from 'aws-cdk-lib/aws-sqs'
import {Duration, RemovalPolicy, Stack} from 'aws-cdk-lib'
import {Optional} from '@othree.io/optional'
import {AppContext, StackAccount} from './core'

/**
 * @module sqs
 */

/**
 * Represents input configuration for an Amazon SQS queue.
 * @interface
 */
export interface QueueInput {
    readonly id: string
    readonly dlq?: DeadLetterQueue
}

/**
 * Creates a function that returns default configuration settings for an Amazon SQS queue.
 *
 * @param {AppContext} context - The App context to determine configuration settings.
 * @returns {Partial<QueueProps>} Default configuration settings for an Amazon SQS queue.
 */
export const withDefaultConfiguration = (context: AppContext): Partial<QueueProps> => {
    return {
        retentionPeriod: Duration.days(14),
        removalPolicy: context.volatile ? RemovalPolicy.DESTROY : RemovalPolicy.RETAIN,
    }
}

/**
 * Creates a function that configures and returns an Amazon SQS queue instance.
 *
 * @param {Partial<QueueProps>} defaultConfiguration - Default configuration settings for the queue.
 * @returns A curried function that returns an Amazon SQS queue instance.
 */
export const withQueue = (defaultConfiguration: Partial<QueueProps>) =>
    /**
     * Creates a function that configures and returns an Amazon SQS queue instance.
     *
     * @param {Stack} stack - The AWS CloudFormation stack to which the resources will be added.
     * @returns A curried function that returns an Amazon SQS queue instance.
     */
    (stack: Stack) =>
        /**
         * Configures the Amazon SQS queue instance.
         *
         * @param {QueueInput} input - Input settings for the Amazon SQS queue.
         * @param {Partial<QueueProps>} [configuration] - Optional configuration for the queue.
         * @returns {IQueue} The configured Amazon SQS queue instance.
         */
        (input: QueueInput, configuration?: Partial<QueueProps>): IQueue => {
            const queue = new Queue(stack, input.id, {
                ...defaultConfiguration,
                queueName: input.id,
                deadLetterQueue: input.dlq,
                ...Optional(configuration).orElse({}),
            })
            return queue
        }

/**
 * Creates a function that generates an Amazon SQS queue ARN from an account and queue name.
 *
 * @param {StackAccount} account - The AWS account information, including region and account ID.
 * @returns A function that takes a queue name and returns the corresponding Amazon SQS queue ARN.
 */
export const queueArnFromName = (account: StackAccount) =>
    /**
     * Generates an Amazon SQS queue ARN from a queue name.
     *
     * @param {string} queueName - The name of the Amazon SQS queue.
     * @returns {string} The Amazon SQS queue ARN generated from the provided queue name and account information.
     */
    (queueName: string): string => {
        return `arn:aws:sqs:${account.region}:${account.accountId}:${queueName}`
    }
