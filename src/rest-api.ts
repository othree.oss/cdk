import {
    BasePathMapping, Cors,
    DomainName,
    IResource,
    IRestApi,
    LambdaIntegration,
    MethodOptions,
    RestApi, UsagePlan,
} from 'aws-cdk-lib/aws-apigateway'
import {Optional} from '@othree.io/optional'
import {IFunction} from 'aws-cdk-lib/aws-lambda'
import {RestApiProps} from 'aws-cdk-lib/aws-apigateway'
import {Stack} from 'aws-cdk-lib'
import {compose, ComposerFn} from './utils/compose'
import {addApiStageToUsagePlan} from './usage-plan'

/**
 * @module restApi
 */

/**
 * Represents a REST API domain configuration.
 * @interface
 */
export interface RestApiDomain {
    readonly domain: string
    readonly basePath: string
}

/**
 * Represents input configuration for a REST API.
 * @interface
 */
export interface RestApiInput {
    readonly apiName: string
}

/**
 * Represents configuration options for a REST API.
 * Extends the partial properties of RestApiProps.
 * @interface
 */
export interface RestApiConfiguration extends Partial<RestApiProps> {
    readonly domain?: RestApiDomain
    readonly usagePlanId?: string
}

/**
 * Creates a default configuration for a REST API.
 *
 * @returns {RestApiConfiguration} The default REST API configuration.
 */
export const withDefaultApiConfiguration = (): RestApiConfiguration => {
    return {
        defaultCorsPreflightOptions: {
            allowHeaders: Cors.DEFAULT_HEADERS,
            allowMethods: ['GET'],
            allowOrigins: Cors.ALL_ORIGINS,
        }
    }
}

/**
 * Creates a function that configures and returns a REST API instance.
 *
 * @param {RestApiConfiguration} defaultConfiguration - The default configuration for the REST API.
 * @returns A curried function returns a configured REST API instance.
 */
export const withRestApi = (defaultConfiguration: RestApiConfiguration) =>
    /**
     * Configures and returns a REST API instance.
     *
     * @param {Stack} stack - The AWS CloudFormation stack to which the resources will be added.
     * @returns A curried function returns a configured REST API instance.
     */
    (stack: Stack) =>
        /**
         * Configures the REST API and its associated resources.
         *
         * @param {RestApiInput} input - Input settings for the REST API.
         * @param {Partial<RestApiProps>} [configuration] - Optional configuration for the REST API.
         * @returns {RestApi} The configured REST API instance.
         */
        (input: RestApiInput, configuration?: Partial<RestApiProps>): RestApi => {
            const restApi = new RestApi(stack, input.apiName, {
                restApiName: input.apiName,
                ...defaultConfiguration,
                ...Optional(configuration!).orElse({}),
            })

            if (defaultConfiguration.domain) {
                const domain = DomainName.fromDomainNameAttributes(stack, `${input.apiName}-Domain`, {
                    domainName: defaultConfiguration.domain.domain,
                } as any)

                new BasePathMapping(stack, `${input.apiName}-PathMapping`, {
                    basePath: defaultConfiguration.domain.basePath,
                    domainName: domain,
                    restApi: restApi,
                })
            }



            Optional(defaultConfiguration.usagePlanId)
                .map(usagePlanId => {
                    return addApiStageToUsagePlan(stack, `${input.apiName}-UsagePlanConstruct`, {
                        ApiId: restApi.restApiId,
                        UsagePlanId: usagePlanId,
                        Stage: restApi.deploymentStage.stageName
                    })
                })

            return restApi
        }

/**
 * Represents a new resource configuration.
 * @interface
 */
interface NewResource {
    readonly path: string
    readonly newResource?: IResource
    readonly resources: {
        [key: string]: IResource
    }
}

/**
 * Enum representing HTTP verbs.
 * @enum {string}
 */
export enum HttpVerb {
    GET = 'GET',
    DELETE = 'DELETE',
    POST = 'POST',
    PUT = 'PUT'
}

/**
 * Represents configuration options for a REST method.
 * @interface
 */
export interface RestConfiguration {
    readonly verb: HttpVerb
    readonly fn: IFunction
    readonly props?: Partial<MethodOptions>
}

/**
 * Creates a function that configures and returns an updated ComposedRestApi instance with added resources and methods.
 *
 * @param {string} path - The path of the new resource.
 * @param {RestConfiguration} configuration - Configuration options for the REST method.
 * @returns A curried function that returns an updated instance with added resources and methods.
 */
export const withResource = (path: string, configuration: RestConfiguration) =>
    /**
     * Configures the ComposedRestApi instance with added resources and methods.
     *
     * @param {ComposedRestApi} api - The ComposedRestApi instance to be updated.
     * @returns {ComposedRestApi} The updated ComposedRestApi instance with added resources and methods.
     */
    (api: ComposedRestApi): ComposedRestApi => {
        const addResource = (resource: string, fullPath: string, parent: IResource, api: ComposedRestApi): IResource => {
            const maybeResource = Optional(api.resources[fullPath])
            if (maybeResource.isEmpty) {
                return parent.addResource(resource)
            }
            return maybeResource.get()
        }

        const resources = path.startsWith('/')
            ? ['/', ...path.split('/').filter(Boolean)]
            : path.split('/')

        const newResource: NewResource = resources.reduce((fullPath: any, resource: string, index: number): NewResource => {
            const parent = index === 0 ? api.api.root : fullPath.resources[fullPath.path]
            const pathSeparator = fullPath.path.endsWith('/') ? '' : '/'
            const path = index === 0 ? resource : `${fullPath.path}${pathSeparator}${resource}`
            const newResource = resource === '/' ? api.api.root : addResource(resource, path, parent, api)
            return {
                path: path,
                newResource: newResource,
                resources: {
                    ...fullPath.resources,
                    [path]: newResource,
                },
            }
        }, {
            path: '',
            newResource: undefined,
            resources: {},
        } as NewResource)

        const lambdaIntegration = new LambdaIntegration(configuration.fn)
        newResource.newResource!.addMethod(configuration.verb, lambdaIntegration, configuration.props)

        return {
            resources: {
                ...api.resources,
                ...newResource.resources,
            },
            api: api.api,
        }
    }

/**
 * Represents a composed REST API instance with associated resources.
 * @interface
 */
export interface ComposedRestApi {
    readonly api: IRestApi
    readonly resources: {
        [key: string]: IResource
    }
}

/**
 * Composes an AWS API Gateway REST API with provided composer functions.
 *
 * @param {IRestApi} api - The AWS API Gateway REST API to be composed.
 * @param {...Array<ComposerFn<ComposedRestApi>>} args - An array of composer functions that modify the ComposedRestApi instance.
 * @returns {ComposedRestApi} The composed ComposedRestApi instance.
 */
export const composeRestApi = (api: IRestApi, ...args: Array<ComposerFn<ComposedRestApi>>): ComposedRestApi => {
    const composedRestApi: ComposedRestApi = {
        api: api,
        resources: {},
    }

    return compose(composedRestApi, ...args)
}
