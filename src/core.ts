import {Optional} from '@othree.io/optional'
import {App} from 'aws-cdk-lib'

/**
 * @module core
 */

/**
 * The maximum length allowed for an ID.
 */
const MaxIdLength: number = 65

/**
 * Represents the context for an Application.
 */
export interface AppContext {
    readonly env: string
    readonly volatile: boolean
    readonly optimize: boolean
    readonly shareCommonDataStores: boolean
}

/**
 * Represents the input for generating an ID.
 */
export interface IdInput {
    readonly prefix?: string
    readonly suffix?: string
}

/**
 * Generates an ID input for Cocoa based on the provided App context.
 *
 * @param context - The App context to use for generating the ID input.
 * @returns {IdInput} An ID input object with prefix and suffix properties.
 */
export const withCocoaIdInput = (context: AppContext): IdInput => {
    return  {
        prefix: 'Cocoa',
        suffix: context.env
    }
}

/**
 * Generates an ID based on the provided input and optional version.
 *
 * @param version - An optional version string to append to the ID.
 * @returns A function that generates an ID based on input and identifier.
 */
export const id = (version?: string) =>
    /**
     * Generates an ID based on the provided input and identifier.
     *
     * @param input - The input configuration for the ID generation.
     * @returns The generated ID string.
     */
    (input: IdInput) =>
        /**
         * Generates the final ID based on the provided identifier.
         *
         * @param identifier - The main identifier for the ID.
         * @returns The generated ID string.
         * @throws Error if the generated ID exceeds the maximum length.
         */
        (identifier: string) => {
            const id = `${Optional(input.prefix).map(_ => `${_}-`).orElse('')}${identifier}${Optional(input.suffix).map(_ => `-${_}`).orElse('')}${Optional(version).map(_ => `-${_!.toUpperCase()}`).orElse('')}`

            if(id.length > MaxIdLength) {
                throw new Error(`Id should be ${MaxIdLength} characters or less`)
            }

            return id
        }

/**
 * Retrieves the App context configuration from the provided app.
 *
 * @param app - The CDK app instance from which to retrieve context values.
 * @returns The App context configuration.
 */
export const appContext = (app: App) : AppContext => {
    return {
        env: app.node.tryGetContext('env'),
        volatile: app.node.tryGetContext('volatile') === 'true',
        optimize: app.node.tryGetContext('optimize') === 'true',
        shareCommonDataStores: app.node.tryGetContext('shareCommonDataStores') === 'true',
    }
}

/**
 * Represents the AWS account and region of a stack.
 */
export interface StackAccount {
    readonly accountId: string
    readonly region: string
}

/**
 * Retrieves the AWS account and region information from environment variables.
 *
 * @returns The AWS account and region information as a `StackAccount` object.
 */
export const getStackAccountFromEnv = (): StackAccount => {
    return {
        accountId: String(process.env.CDK_DEFAULT_ACCOUNT),
        region: String(process.env.CDK_DEFAULT_REGION)
    }
}
