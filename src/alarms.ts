import {AppContext} from './core'
import {Optional} from '@othree.io/optional'
import {ITopic, Topic} from 'aws-cdk-lib/aws-sns'
import {App, Duration, Stack} from 'aws-cdk-lib'
import {Alarm, AlarmProps, ComparisonOperator, Metric, TreatMissingData} from 'aws-cdk-lib/aws-cloudwatch'
import {SnsAction} from 'aws-cdk-lib/aws-cloudwatch-actions'
import {Queue} from 'aws-cdk-lib/aws-sqs'
import {Function} from 'aws-cdk-lib/aws-lambda'
import {RestApi} from 'aws-cdk-lib/aws-apigateway'

/**
 * @module alarms
 */

/**
 * Represents the context for alarms.
 */
export interface AlarmContext {
    readonly criticalAlarmTopic: ITopic
    readonly mildAlarmTopic: ITopic
}

/**
 * Represents the input configuration for creating an AlarmContext.
 */
export interface AlarmContextInput {
    readonly criticalAlarmTopicArn: string
    readonly mildAlarmTopicArn: string
}

/**
 * Generates an AlarmContext based on the provided inputs.
 *
 * @param {Function} withId - Function to generate an ID.
 * @returns A function that generates an AlarmContext.
 */
export const withAlarmsContext = (withId: (id: string) => string) =>
    /**
     * Generates an AlarmContext based on the provided inputs.
     *
     * @param {App} app - The CDK app.
     * @param {Stack} stack - The CDK stack.
     * @param {string} project - The project name.
     * @param {AlarmContextInput} alarmContextInput - The input for alarm context.
     * @returns {AlarmContext} The generated AlarmContext.
     */
    (app: App, stack: Stack, project: string, alarmContextInput: AlarmContextInput): AlarmContext => {
        return {
            criticalAlarmTopic: Topic.fromTopicArn(stack, withId(`${project}-CriticalAlarmTopic`), alarmContextInput.criticalAlarmTopicArn),
            mildAlarmTopic: Topic.fromTopicArn(stack, withId(`${project}-MildAlarmTopic`), alarmContextInput.mildAlarmTopicArn),
        }
    }

/**
 * Generates an alarm ID based on the provided AppContext.
 *
 * @param {AppContext} appContext - The AppContext.
 * @returns A curried function that generates an alarm ID.
 */
export const withAlarmId = (appContext: AppContext) =>
    /**
     * Generates an alarm ID based on the provided project, alarm source, and alarm name.
     *
     * @param {string} project - The source of the alarm.
     * @returns @returns {Function} A curried function that generates an alarm ID.
     */
    (project: string) =>
        /**
         * Generates an alarm ID based on the provided project, alarm source, and alarm name.
         *
         * @param {string} alarmSource - The source of the alarm.
         * @param {string} alarmName - The name of the alarm.
         * @returns {string} The generated alarm ID.
         */
        (alarmSource: string, alarmName: string): string => {
            return `[${project}-${alarmSource}] ${alarmName} [${appContext.env}]`
        }

/**
 * Represents the type of alarm.
 */
export enum AlarmType {
    CRITICAL = 'CRITICAL',
    MILD = 'MILD'
}

/**
 * Represents the input for creating an alarm.
 */
export interface AlarmInput {
    readonly alarmId: string
    readonly alarmType: AlarmType
}

/**
 * Creates an AWS CloudWatch alarm based on the provided inputs.
 *
 * @param {Stack} stack - The AWS CDK stack.
 * @returns A curried function that creates an alarm.
 */
export const withAlarm = (stack: Stack) =>
    /**
     * Creates an AWS CloudWatch alarm based on the provided alarm context and inputs.
     *
     * @param {AlarmContext} alarmContext - The context for alarms.
     * @returns A curried function that creates an alarm.
     */
    (alarmContext: AlarmContext) =>
        /**
         * Creates an AWS CloudWatch alarm based on the provided alarm input and properties.
         *
         * @param {AlarmInput} input - The input for the alarm.
         * @param {AlarmProps} props - The properties for the alarm.
         * @returns {Alarm} The created AWS CloudWatch alarm.
         */
        (input: AlarmInput, props: AlarmProps): Alarm => {
            const alarm = new Alarm(stack, input.alarmId, {
                ...props,
            })
            alarm.addAlarmAction(new SnsAction(input.alarmType === AlarmType.CRITICAL ? alarmContext.criticalAlarmTopic : alarmContext.mildAlarmTopic))

            return alarm
        }

/**
 * Generates an alarm for the projection dead-letter queue.
 *
 * @param {Function} withId - A function to generate IDs.
 * @returns A curried function that generates a projection DLQ alarm.
 */
export const withProjectionDLQAlarm = (withId: (id: string) => string) =>
    /**
     * Generates an alarm for the projection dead-letter queue.
     *
     * @param {Function} withAlarmId - A function to generate the alarm ID.
     * @returns A curried function that generates a projection DLQ alarm.
     */
    (withAlarmId: (alarmSource: string, alarmName: string) => string) =>
        /**
         * Generates an alarm for the projection dead-letter queue based on the provided CDK stack.
         *
         * @param {Stack} stack - The AWS CDK stack.
         * @returns A curried function that generates a projection DLQ alarm.
         */
        (stack: Stack) =>
            /**
             * Generates an alarm for the projection dead-letter queue based on the provided alarm context.
             *
             * @param {AlarmContext} alarmContext - The context for alarms.
             * @returns A curried function that generates a projection DLQ alarm.
             */
            (alarmContext: AlarmContext) =>
                /**
                 * Generates an alarm for the projection dead-letter queue based on the provided ID and dead-letter queue ARN.
                 *
                 * @param {string} id - The ID for the alarm.
                 * @param {string} dlqArn - The ARN of the dead-letter queue.
                 * @returns {Alarm} The generated projection DLQ alarm.
                 */
                (id: string, dlqArn: string): Alarm => {
                    const dlq = Queue.fromQueueArn(stack, withId(id), dlqArn)

                    return withAlarm(stack)(alarmContext)({
                        alarmId: withId(`${id}-Alarm`),
                        alarmType: AlarmType.CRITICAL,
                    }, {
                        metric: dlq.metric('ApproximateNumberOfMessagesVisible', {
                            statistic: 'Sum',
                            period: Duration.minutes(1),
                        }),
                        alarmName: withAlarmId('ProjectionDLQ', 'HasMessages'),
                        alarmDescription: 'Critical alarm. This should never get triggered. Finding messages in this queue means that the projection might be outdated/out of sync.',
                        threshold: 1,
                        evaluationPeriods: 1,
                        datapointsToAlarm: 1,
                        treatMissingData: TreatMissingData.NOT_BREACHING,
                    })
                }

/**
 * Represents input for creating an SQS error alarm.
 */
export interface SqsErrorAlarmInput {
    readonly id: string
    readonly alarmType: AlarmType,
    readonly alarmDescription: string,
    readonly alarmProps?: Partial<AlarmProps>
    readonly sqsArn: string,
    readonly sqsThreshold: number,
    readonly sqsEvaluationPeriod: number,
    readonly sqsDatapointsToAlarm: number
}

/**
 * Generates an SQS error alarm based on the provided inputs.
 *
 * @param {function(string): string} withId - A function to generate IDs.
 * @returns A curried function that generates an SQS error alarm.
 */
export const withSqsAlarm = (withId: (id: string) => string) =>
    /**
     * Generates an SQS error alarm based on the provided function for generating alarm IDs.
     *
     * @param {Function} withAlarmId - A function to generate alarm IDs.
     * @returns A curried function that generates an SQS error alarm.
     */
    (withAlarmId: (alarmSource: string, alarmName: string) => string) =>
        /**
         * Generates an SQS error alarm based on the provided AWS CDK stack.
         *
         * @param {Stack} stack - The AWS CDK stack.
         * @returns A curried function that generates an SQS error alarm.
         */
        (stack: Stack) =>
            /**
             * Generates an SQS error alarm based on the provided alarm context.
             *
             * @param {AlarmContext} alarmContext - The context for alarms.
             * @returns A curried function that generates an SQS error alarm.
             */
            (alarmContext: AlarmContext) =>
                /**
                 * Generates an SQS error alarm based on the provided input.
                 *
                 * @param {SqsErrorAlarmInput} input - The input for the SQS error alarm.
                 * @returns {Alarm} The generated SQS error alarm.
                 */
                (input: SqsErrorAlarmInput): Alarm => {
                    const dlq = Queue.fromQueueArn(stack, withId(input.id), input.sqsArn)

                    return withAlarm(stack)(alarmContext)({
                        alarmId: withId(`${input.id}-Alarm`),
                        alarmType: input.alarmType,
                    }, {
                        metric: dlq.metric('ApproximateNumberOfMessagesVisible', {
                            statistic: 'Sum',
                            period: Duration.minutes(1),
                        }),
                        alarmName: withAlarmId('SQS', 'HasMessages'),
                        alarmDescription: input.alarmDescription,
                        threshold: input.sqsThreshold,
                        evaluationPeriods: input.sqsEvaluationPeriod,
                        datapointsToAlarm: input.sqsDatapointsToAlarm,
                        treatMissingData: TreatMissingData.NOT_BREACHING,
                        ...input.alarmProps,
                    })
                }

/**
 * Represents input for creating a Lambda error alarm.
 */
export interface LambdaErrorAlarmInput {
    readonly id: string
    readonly alarmType: AlarmType,
    readonly lambdaArn: string,
    readonly alarmDescription: string,
    readonly alarmProps?: Partial<AlarmProps>
}

/**
 * Generates a Lambda error alarm based on the provided inputs.
 *
 * @param {function(string): string} withId - A function to generate IDs.
 * @returns A curried function that generates a Lambda error alarm.
 */
export const withLambdaErrorsAlarm = (withId: (id: string) => string) =>
    /**
     * Generates a Lambda error alarm based on the provided function for generating alarm IDs.
     *
     * @param {Function} withAlarmId - A function to generate alarm IDs.
     * @returns A curried function that generates a Lambda error alarm.
     */
    (withAlarmId: (alarmSource: string, alarmName: string) => string) =>
        /**
         * Generates a Lambda error alarm based on the provided AWS CDK stack.
         *
         * @param {Stack} stack - The AWS CDK stack.
         * @returns A curried function that generates a Lambda error alarm.
         */
        (stack: Stack) =>
            /**
             * Generates a Lambda error alarm based on the provided alarm context.
             *
             * @param {AlarmContext} alarmContext - The context for alarms.
             * @returns A curried function that generates a Lambda error alarm.
             */
            (alarmContext: AlarmContext) =>
                /**
                 * Generates a Lambda error alarm based on the provided input.
                 *
                 * @param {LambdaErrorAlarmInput} input - The input for the Lambda error alarm.
                 * @returns {Alarm} The generated Lambda error alarm.
                 */
                (input: LambdaErrorAlarmInput): Alarm => {

                    const lambdaHandler = Function.fromFunctionArn(stack, withId(input.id), input.lambdaArn)

                    return withAlarm(stack)(alarmContext)({
                        alarmId: withId(`${input.id}-Alarm`),
                        alarmType: AlarmType.CRITICAL,
                    }, {
                        metric: lambdaHandler.metricErrors(),
                        alarmName: withAlarmId(input.lambdaArn, 'HasErrors'),
                        alarmDescription: input.alarmDescription,
                        threshold: 1,
                        evaluationPeriods: 1,
                        datapointsToAlarm: 1,
                        comparisonOperator: ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
                        treatMissingData: TreatMissingData.NOT_BREACHING,
                        ...input.alarmProps,
                    })
                }

/**
 * Represents input for creating a REST API error alarm.
 */
export interface RestApiErrorAlarmInput {
    readonly id: string
    readonly alarmType: AlarmType,
    readonly apiId: string,
    readonly alarmDescription: string,
    readonly alarmThreshold: number,
    readonly alarmEvaluationPeriods: number,
    readonly alarmDatapoints: number,
    readonly alarmProps?: Partial<AlarmProps>
}

/**
 * Generates a REST API error alarm based on the provided inputs.
 *
 * @param {Function} withId - A function to generate IDs.
 * @returns A curried function that generates a REST API error alarm.
 */
const withRestApiErrorsAlarm = (withId: (id: string) => string) =>
    /**
     * Generates a REST API error alarm based on the provided function for generating alarm IDs.
     *
     * @param {Function} withAlarmId - A function to generate alarm IDs.
     * @returns A curried function that generates a REST API error alarm.
     */
    (withAlarmId: (alarmSource: string, alarmName: string) => string) =>
        /**
         * Generates a REST API error alarm based on the provided AWS CDK stack.
         *
         * @param {Stack} stack - The AWS CDK stack.
         * @returns A curried function that generates a REST API error alarm.
         */
        (stack: Stack) =>
            /**
             * Generates a REST API error alarm based on the provided alarm context.
             *
             * @param {AlarmContext} alarmContext - The context for alarms.
             * @returns A curried function that generates a REST API error alarm.
             */
            (alarmContext: AlarmContext) =>
                /**
                 * Generates a REST API error alarm based on the provided input.
                 *
                 * @param {RestApiErrorAlarmInput} input - The input for the REST API error alarm.
                 * @param {Metric} restApiMetric - The metric for the REST API.
                 * @param {string} alarmName - The name of the alarm.
                 * @returns {Alarm} The generated REST API error alarm.
                 */
                (input: RestApiErrorAlarmInput, restApiMetric: Metric, alarmName: string): Alarm => {
                    return withAlarm(stack)(alarmContext)({
                        alarmId: withId(`${input.id}-Alarm`),
                        alarmType: input.alarmType,
                    }, {
                        metric: restApiMetric,
                        alarmName: withAlarmId(input.apiId, alarmName),
                        alarmDescription: input.alarmDescription,
                        threshold: input.alarmThreshold,
                        evaluationPeriods: input.alarmEvaluationPeriods,
                        datapointsToAlarm: input.alarmDatapoints,
                        comparisonOperator: ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
                        treatMissingData: TreatMissingData.NOT_BREACHING,
                        ...input.alarmProps,
                    })
                }

/**
 * Generates a REST API 5XX errors alarm based on the provided inputs.
 *
 * @param {Function} withId - A function to generate IDs.
 * @returns A curried function that generates a REST API 5XX errors alarm.
 */
export const withRestApi5XXErrorsAlarm = (withId: (id: string) => string) =>
    /**
     * Generates a REST API 5XX errors alarm based on the provided function for generating alarm IDs.
     *
     * @param {Function} withAlarmId - A function to generate alarm IDs.
     * @returns A curried function that generates a REST API 5XX errors alarm.
     */
    (withAlarmId: (alarmSource: string, alarmName: string) => string) =>
        /**
         * Generates a REST API 5XX errors alarm based on the provided AWS CDK stack.
         *
         * @param {Stack} stack - The AWS CDK stack.
         * @returns A curried function that generates a REST API 5XX errors alarm.
         */
        (stack: Stack) =>
            /**
             * Generates a REST API 5XX errors alarm based on the provided alarm context.
             *
             * @param {AlarmContext} alarmContext - The context for alarms.
             * @returns A curried function that generates a REST API 5XX errors alarm.
             */
            (alarmContext: AlarmContext) =>
                /**
                 * Generates a REST API 5XX errors alarm based on the provided input.
                 *
                 * @param {RestApiErrorAlarmInput} input - The input for the REST API 5XX errors alarm.
                 * @returns {Alarm} The generated REST API 5XX errors alarm.
                 */
                (input: RestApiErrorAlarmInput): Alarm => {
                    const restApi = RestApi.fromRestApiId(stack, input.id, input.apiId) as RestApi
                    return withRestApiErrorsAlarm(withId)(withAlarmId)(stack)(alarmContext)(input, restApi.metricServerError(), '5XXError')
                }

/**
 * Generates a REST API 4XX errors alarm based on the provided inputs.
 *
 * @param {Function} withId - A function to generate IDs.
 * @returns A curried function that generates a REST API 4XX errors alarm.
 */
export const withRestApi4XXErrorsAlarm = (withId: (id: string) => string) =>
    /**
     * Generates a REST API 4XX errors alarm based on the provided function for generating alarm IDs.
     *
     * @param {Function} withAlarmId - A function to generate alarm IDs.
     * @returns A curried function that generates a REST API 4XX errors alarm.
     */
    (withAlarmId: (alarmSource: string, alarmName: string) => string) =>
        /**
         * Generates a REST API 4XX errors alarm based on the provided AWS CDK stack.
         *
         * @param {Stack} stack - The AWS CDK stack.
         * @returns A curried function that generates a REST API 4XX errors alarm.
         */
        (stack: Stack) =>
            /**
             * Generates a REST API 4XX errors alarm based on the provided alarm context.
             *
             * @param {AlarmContext} alarmContext - The context for alarms.
             * @returns A curried function that generates a REST API 4XX errors alarm.
             */
            (alarmContext: AlarmContext) =>
                /**
                 * Generates a REST API 4XX errors alarm based on the provided input.
                 *
                 * @param {RestApiErrorAlarmInput} input - The input for the REST API 4XX errors alarm.
                 * @returns {Alarm} The generated REST API 4XX errors alarm.
                 */
                (input: RestApiErrorAlarmInput): Alarm => {
                    const restApi = RestApi.fromRestApiId(stack, input.id, input.apiId) as RestApi
                    return withRestApiErrorsAlarm(withId)(withAlarmId)(stack)(alarmContext)(input, restApi.metricClientError(), '4XXError')
                }

/**
 * Represents input for creating a Function alarm.
 */
export interface FunctionErrorAlarmInput {
    readonly projectName: string
    readonly functionName: string
    readonly alarmType: AlarmType
    readonly alarmThreshold: number
    readonly alarmEvaluationPeriods: number
    readonly alarmDatapoints: number
    readonly alarmProps?: Partial<AlarmProps>
    readonly metricDuration?: number
}

/**
 * Generates an alarm for a specific function based on the provided inputs.
 *
 * @param {Function} withId - A function to generate IDs.
 * @returns A curried function that generates an alarm for a specific function.
 */
const withFunctionAlarm = (withId: (id: string) => string) =>
    /**
     * Generates an alarm for a specific function based on the provided function for generating alarm IDs.
     *
     * @param {Function} withAlarmId - A function to generate alarm IDs.
     * @returns A curried function that generates an alarm for a specific function.
     */
    (withAlarmId: (alarmSource: string, alarmName) => string) =>
        /**
         * Generates an alarm for a specific function based on the provided AWS CDK stack.
         *
         * @param {Stack} stack - The AWS CDK stack.
         * @returns A curried function that generates an alarm for a specific function.
         */
        (stack: Stack) =>
            /**
             * Generates an alarm for a specific function based on the provided alarm context.
             *
             * @param {AlarmContext} alarmContext - The context for alarms.
             * @returns A curried function that generates an alarm for a specific function.
             */
            (alarmContext: AlarmContext) =>
                /**
                 * Generates an alarm for a specific function based on the provided metric namespace.
                 *
                 * @param {string} metricNamespace - The namespace for the metric.
                 * @returns A curried function that generates an alarm for a specific function.
                 */
                (metricNamespace: string) =>
                    /**
                     * Generates an alarm for a specific function based on the provided input, metric name, and alarm description.
                     *
                     * @param {FunctionErrorAlarmInput} input - The input for the function error alarm.
                     * @param {string} metricName - The name of the metric.
                     * @param {string} alarmDescription - The description of the alarm.
                     * @returns {Alarm} The generated function error alarm.
                     */
                    (input: FunctionErrorAlarmInput, metricName: string, alarmDescription: string): Alarm => {

                        return withAlarm(stack)(alarmContext)({
                            alarmId: withId(`${input.projectName}-${input.functionName}-${metricName}`),
                            alarmType: input.alarmType,
                        }, {
                            metric: new Metric({
                                namespace: metricNamespace,
                                metricName: metricName,
                                dimensionsMap: {['Function']: input.functionName},
                                statistic: 'Sum',
                                period: Optional(input.metricDuration).map(_ => Duration.minutes(_)).orElse(Duration.minutes(5)),
                            }),
                            alarmName: withAlarmId(input.projectName, `${input.functionName}${metricName}`),
                            alarmDescription: alarmDescription,
                            threshold: input.alarmThreshold,
                            evaluationPeriods: input.alarmEvaluationPeriods,
                            datapointsToAlarm: input.alarmDatapoints,
                            comparisonOperator: ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
                            treatMissingData: TreatMissingData.NOT_BREACHING,
                            ...input.alarmProps,
                        })

                    }

/**
 * Generates an alarm for a function based on the provided inputs for HTTP request errors.
 *
 * @param {Function} withId - A function to generate IDs.
 * @returns A curried function that generates an alarm for function HTTP request errors.
 */
export const withFunctionHttpRequestErrorAlarm = (withId: (id: string) => string) =>
    /**
     * Generates an alarm for a function based on the provided function for generating alarm IDs for HTTP request errors.
     *
     * @param {Function} withAlarmId - A function to generate alarm IDs.
     * @returns A curried function that generates an alarm for function HTTP request errors.
     */
    (withAlarmId: (alarmSource: string, alarmName) => string) =>
        /**
         * Generates an alarm for a function based on the provided AWS CDK stack for HTTP request errors.
         *
         * @param {Stack} stack - The AWS CDK stack.
         * @returns A curried function that generates an alarm for function HTTP request errors.
         */
        (stack: Stack) =>
            /**
             * Generates an alarm for a function based on the provided alarm context for HTTP request errors.
             *
             * @param {AlarmContext} alarmContext - The context for alarms.
             * @returns A curried function that generates an alarm for function HTTP request errors.
             */
            (alarmContext: AlarmContext) =>
                /**
                 * Generates an alarm for a function based on the provided metric namespace for HTTP request errors.
                 *
                 * @param {string} metricNamespace - The namespace for the metric.
                 * @returns A curried function that generates an alarm for function HTTP request errors.
                 */
                (metricNamespace: string) =>
                    /**
                     * Generates an alarm for a function based on the provided input for HTTP request errors.
                     *
                     * @param {FunctionErrorAlarmInput} input - The input for the function error alarm.
                     * @returns {Alarm} The generated function error alarm.
                     */
                    (input: FunctionErrorAlarmInput): Alarm => {
                        const metricName = '5XX'
                        const alarmDescription = `This alarm gets triggered when the function's: ${input.functionName} http call in ${input.projectName} responds with 5XX errors`
                        return withFunctionAlarm(withId)(withAlarmId)(stack)(alarmContext)(metricNamespace)(input, metricName, alarmDescription)
                    }

/**
 * Generates an alarm for a function based on the provided inputs for call count errors.
 *
 * @param {Function} withId - A function to generate IDs.
 * @returns A curried function that generates an alarm for function call count errors.
 */
export const withFunctionCountAlarm = (withId: (id: string) => string) =>
    /**
     * Generates an alarm for a function based on the provided function for generating alarm IDs for call count errors.
     *
     * @param {Function} withAlarmId - A function to generate alarm IDs.
     * @returns A curried function that generates an alarm for function call count errors.
     */
    (withAlarmId: (alarmSource: string, alarmName: string) => string) =>
        /**
         * Generates an alarm for a function based on the provided AWS CDK stack for call count errors.
         *
         * @param {Stack} stack - The AWS CDK stack.
         * @returns A curried function that generates an alarm for function call count errors.
         */
        (stack: Stack) =>
            /**
             * Generates an alarm for a function based on the provided alarm context for call count errors.
             *
             * @param {AlarmContext} alarmContext - The context for alarms.
             * @returns A curried function that generates an alarm for function call count errors.
             */
            (alarmContext: AlarmContext) =>
                /**
                 * Generates an alarm for a function based on the provided metric namespace for call count errors.
                 *
                 * @param {string} metricNamespace - The namespace for the metric.
                 * @returns A curried function that generates an alarm for function call count errors.
                 */
                (metricNamespace: string) =>
                    /**
                     * Generates an alarm for a function based on the provided input for call count errors.
                     *
                     * @param {FunctionErrorAlarmInput} input - The input for the function error alarm.
                     * @returns {Alarm} The generated function error alarm.
                     */
                    (input: FunctionErrorAlarmInput): Alarm => {
                        const metricName = 'Count'
                        const alarmDescription = `This alarm gets triggered when the function: ${input.functionName} in ${input.projectName} call count is in the alarm range`
                        return withFunctionAlarm(withId)(withAlarmId)(stack)(alarmContext)(metricNamespace)(input, metricName, alarmDescription)
                    }
