import {
    AuroraCapacityUnit,
    Credentials,
    DatabaseClusterEngine,
    DatabaseSecret,
    IParameterGroup,
    ParameterGroup,
    ServerlessCluster,
    ServerlessScalingOptions,
    ServerlessClusterProps,
    IClusterEngine,
} from 'aws-cdk-lib/aws-rds'
import {Optional} from '@othree.io/optional'
import {IVpc, SelectedSubnets} from 'aws-cdk-lib/aws-ec2'
import {App, Stack} from 'aws-cdk-lib'

/**
 * @module rds
 */

/**
 * Configuration settings for a serverless Amazon RDS instance.
 */
export interface ServerlessRdsConfiguration {
    readonly username: string
    readonly defaultDatabaseName: string
    readonly scaling: ServerlessScalingOptions
}

/**
 * Input parameters for creating a serverless Amazon RDS instance.
 */
export interface ServerlessRdsInput {
    readonly clusterName: string
    readonly selectedSubnets: SelectedSubnets
    readonly vpc: IVpc
    readonly credentials: Credentials
}

/**
 * Configuration for the database engine and parameter group in Amazon RDS.
 */
export interface EngineConfiguration {
    readonly engine: IClusterEngine
    readonly parameterGroup: IParameterGroup
}

/**
 * Generates a configuration for a serverless Amazon RDS database.
 *
 * @param {ServerlessScalingOptions} serverlessScalingOptions - Configuration options for serverless database scaling.
 * @returns A function that takes database name and username as input and returns a `ServerlessRdsConfiguration` object.
 */
export const withServerlessRdsConfiguration = (serverlessScalingOptions: ServerlessScalingOptions) =>
    /**
     * Generates a configuration for a serverless Amazon RDS database.
     *
     * @param {string} databaseName - The name of the default database.
     * @param {string} username - The username for accessing the database.
     * @returns A `ServerlessRdsConfiguration` object.
     */
    (databaseName: string, username: string): ServerlessRdsConfiguration => {
        return {
            username: username,
            defaultDatabaseName: databaseName,
            scaling: serverlessScalingOptions,
        }
    }


/**
 * Generates an engine configuration for an Amazon Aurora PostgreSQL 10 database cluster.
 *
 * @param {Stack} stack - The AWS CloudFormation stack where the resources will be provisioned.
 * @returns An `EngineConfiguration` object.
 */
export const withAuroraPostgresSql10EngineConfiguration = (stack: Stack) =>
    /**
     * Generates an engine configuration for an Amazon Aurora PostgreSQL 10 database cluster.
     *
     * @param withId - A function to generate resource IDs based on an identifier.
     * @returns {EngineConfiguration} An `EngineConfiguration` object.
     */
    (withId: (identifier: string) => string): EngineConfiguration => {
        return {
            engine: DatabaseClusterEngine.AURORA_POSTGRESQL,
            parameterGroup: ParameterGroup.fromParameterGroupName(stack, withId('DefaultAuroraPostgresql10'), 'default.aurora-postgresql10'),
        }
    }

/**
 * Generates a serverless scaling configuration for an Amazon Aurora Serverless database cluster.
 *
 * @param {App} app - The CDK app where the context is defined.
 * @returns A function that returns the Serverless scaling configuration
 */
export const withServerlessScalingConfiguration = (app: App) =>
    /**
     * Generates a serverless scaling configuration for an Amazon Aurora Serverless database cluster.
     *
     * @param {string} clusterId - The ID of the cluster to fetch scaling configuration for.
     * @returns A `ServerlessScalingOptions` object.
     */
    (clusterId: string): ServerlessScalingOptions => {
        const maybeConfiguration = Optional(app.node.tryGetContext(clusterId)).map(_ => _.scaling)
        return maybeConfiguration
            .map(scalingConfiguration => {
                const keys = Object.keys(scalingConfiguration)
                if (keys.includes('minCapacity') && keys.includes('maxCapacity')) {
                    return {
                        minCapacity: AuroraCapacityUnit[AuroraCapacityUnit[scalingConfiguration.minCapacity]],
                        maxCapacity: AuroraCapacityUnit[AuroraCapacityUnit[scalingConfiguration.maxCapacity]],
                    }
                }

                throw new Error('Invalid scaling configuration')
            })
            .orElse({
                minCapacity: AuroraCapacityUnit.ACU_2,
                maxCapacity: AuroraCapacityUnit.ACU_4,
            })
    }

/**
 * Creates database credentials as a secret for a database resource.
 *
 * @param stack - The CDK stack where the secret will be created.
 * @returns A function that generates a `DatabaseSecret` object.
 */
export const withDatabaseCredentials = (stack: Stack) =>
    /**
     * Creates database credentials as a secret for a database resource.
     *
     * @param {string} name - The name of the secret.
     * @param {string} username - The username for the database.
     * @returns {DatabaseSecret} A `DatabaseSecret` object representing the database credentials.
     */
    (name: string, username: string): DatabaseSecret => {
        return new DatabaseSecret(stack, name, {
            secretName: name,
            username: username,
        })
    }

/**
 * Creates a Serverless Aurora RDS cluster and returns the cluster instance.
 *
 * @param {Stack} stack - The AWS CloudFormation stack to which the resources will be added.
 * @returns A function that returns a function to create a Serverless Aurora RDS cluster.
 */
export const withServerlessRds = (stack: Stack) =>
    /**
     * Creates a function that generates a unique identifier using the provided identifier.
     *
     * @param withId - Function to generate a full identifier based on a partial identifier.
     * @returns A function that returns a function to create a Serverless Aurora RDS cluster.
     */
    (withId: (identifier: string) => string) =>
        /**
         * Creates a function that configures the Serverless Aurora RDS cluster.
         *
         * @param {ServerlessRdsConfiguration} serverlessRdsConfiguration - Configuration options for the Serverless Aurora RDS cluster.
         * @returns A function that returns a function to create a Serverless Aurora RDS cluster.
         */
        (serverlessRdsConfiguration: ServerlessRdsConfiguration) =>
            /**
             * Creates a function that configures the engine settings for the Aurora RDS cluster.
             *
             * @param {EngineConfiguration} engineConfiguration - Configuration options for the database engine.
             * @returns A function that returns a function to create a Serverless Aurora RDS cluster.
             */
            (engineConfiguration: EngineConfiguration) =>
                /**
                 * Creates a Serverless Aurora RDS cluster instance.
                 *
                 * @param {ServerlessRdsInput} input - Input settings for creating the cluster.
                 * @param {Partial<ServerlessClusterProps>} [configuration] - Optional configuration for the cluster.
                 * @returns {ServerlessCluster} The created Serverless Aurora RDS cluster instance.
                 */
                (input: ServerlessRdsInput, configuration?: Partial<ServerlessClusterProps>): ServerlessCluster => {
                    const dbServer = new ServerlessCluster(stack, withId(`${input.clusterName}`), {
                        vpcSubnets: input.selectedSubnets,
                        credentials: input.credentials,
                        parameterGroup: engineConfiguration.parameterGroup,
                        vpc: input.vpc,
                        defaultDatabaseName: serverlessRdsConfiguration.defaultDatabaseName,
                        engine: engineConfiguration.engine,
                        scaling: serverlessRdsConfiguration.scaling,
                        ...Optional(configuration).orElse({}),
                    })

                    return dbServer
                }
