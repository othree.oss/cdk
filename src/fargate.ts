import {Optional} from '@othree.io/optional'
import { DockerImageAsset } from 'aws-cdk-lib/aws-ecr-assets'
import {PolicyStatement, Role, ServicePrincipal} from 'aws-cdk-lib/aws-iam'
import {IVpc} from 'aws-cdk-lib/aws-ec2'
import {Stack} from 'aws-cdk-lib'
import {AwsLogDriver, Cluster, ContainerImage, FargateService, FargateTaskDefinition} from 'aws-cdk-lib/aws-ecs'

/**
 * @module fargate
 */

/**
 * Represents the input configuration for creating a Fargate task.
 */
export interface FargateTaskInput {
    readonly name: string
    readonly image: DockerImageAsset
    readonly environment: Record<string, string>
    readonly policies: Array<PolicyStatement>
    readonly vpc?: IVpc
    readonly taskCount: number
}

/**
 * Represents the configuration options for a Fargate task.
 */
export interface FargateTaskConfiguration {
    readonly memoryLimitMiB: number
    readonly cpu: number
    readonly publicIp: boolean
}


/**
 * Creates a Fargate service based on the provided input configuration.
 *
 * @param {Function} withId - A function to transform an identifier.
 * @returns A function chain that takes various dependencies and input parameters to create a Fargate service.
 */
export const withFargateService = (withId: (id: string) => string) =>
    /**
     * Specifies the default configuration for a Fargate task.
     *
     * @param {Stack} stack - The AWS CloudFormation stack where the Fargate service will be created.
     * @returns A function chain that takes input parameters to create a Fargate service.
     */
    (stack: Stack) =>
        /**
         * Specifies the default configuration for a Fargate task.
         *
         * @param {FargateTaskConfiguration} defaultConfiguration - The default configuration for a Fargate task.
         * @returns A function chain that takes input parameters to create a Fargate service.
         */
        (defaultConfiguration: FargateTaskConfiguration) =>
            /**
             * Creates a Fargate service based on the provided input configuration.
             *
             * @param {FargateTaskInput} input - The input configuration for creating the Fargate service.
             * @param {Partial<FargateTaskConfiguration>} configuration - Additional configuration options for the Fargate service.
             * @returns {FargateService} - The created Fargate service.
             */
            (input: FargateTaskInput, configuration?: Partial<FargateTaskConfiguration>): FargateService => {
                const cluster = new Cluster(stack, `${input.name}-Cluster`, {
                    clusterName: withId(`${input.name}-Cluster`),
                    vpc: input.vpc
                })

                const role = new Role(stack, `${input.name}-Role`, {
                    roleName: withId(`${input.name}-Role`),
                    assumedBy: new ServicePrincipal('ecs-tasks.amazonaws.com')
                })
                input.policies.forEach(policy => {
                    role.addToPolicy(policy)
                })

                const task = new FargateTaskDefinition(stack, `${input.name}-Task`, {
                    ...defaultConfiguration,
                    ...Optional(configuration).orElse({}),
                    taskRole: role
                })

                task.addContainer(`${input.name}-Container`, {
                    image: ContainerImage.fromEcrRepository(input.image.repository, input.image.assetHash),
                    environment: input.environment,
                    logging: new AwsLogDriver({
                        streamPrefix: withId(input.name)
                    })
                })

                const service = new FargateService(stack, withId(`${input.name}-Service`), {
                    cluster: cluster,
                    taskDefinition: task,
                    assignPublicIp: Optional(configuration?.publicIp).orElse(defaultConfiguration.publicIp),
                    desiredCount: input.taskCount
                })

                return service
            }
