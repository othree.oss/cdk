import {Optional} from '@othree.io/optional'
import {StackAccount} from './core'
import {
    Code,
    FunctionProps,
    IFunction,
    Runtime,
    Tracing,
    Function,
    Alias,
    FunctionAttributes, Architecture,
} from 'aws-cdk-lib/aws-lambda'
import {App, Duration, Stack} from 'aws-cdk-lib'

/**
 * @module lambda
 */

/**
 * The default alias name for the Lambda function.
 */
const DEFAULT_ALIAS_NAME = 'LATEST'

/**
 * Represents the scaling configuration for an AWS Lambda function.
 */
export interface ScalingConfiguration {
    readonly minCapacity: number
    readonly maxCapacity: number
    readonly utilizationTarget: number
}

/**
 * Represents the input required to create an AWS Lambda function.
 */
export interface LambdaInput {
    readonly functionName: string
    readonly code: Code
    readonly handler: string
    readonly environment?: {
        [key: string]: string;
    }
}

/**
 * Represents the configuration options for an AWS Lambda function.
 */
export interface LambdaConfiguration extends Partial<FunctionProps> {
    readonly runtime: Runtime
    readonly tracing: Tracing
    readonly timeout: Duration
    readonly memorySize: number
    readonly architecture: Architecture
}

/**
 * Generates the default configuration options for an AWS Lambda function based on the provided context.
 *
 * @param {App} app - The CDK app instance.
 * @returns {LambdaConfiguration} - The default configuration options for the Lambda function.
 */
export const withDefaultConfiguration = (app: App): LambdaConfiguration => {
    const lambda = app.node.tryGetContext('lambda') ?? {}
    return {
        runtime: Runtime[lambda.runtime] ?? Runtime.NODEJS_18_X,
        tracing: Tracing[lambda.tracing] ?? Tracing.DISABLED,
        timeout: lambda.timeout ? Duration.seconds(lambda.timeout) : Duration.seconds(30),
        memorySize: lambda.memorySize ? Number(lambda.memorySize) : 256,
        architecture: Architecture[lambda.architecture] ?? Architecture.ARM_64,
    }
}

/**
 * Generates a Lambda configuration based on the provided CDK app context and a default configuration.
 *
 * @param {App} app - The CDK app instance.
 * @returns A function that generates the Lambda configuration for a specific Lambda function.
 */
export const withLambdaConfiguration = (app: App) =>
    /**
     * Generates a Lambda configuration for a specific Lambda function.
     *
     * @param {Function} withDefaultConfiguration - A function that generates the default Lambda configuration.
     * @returns A function that generates the Lambda configuration for a specific Lambda function.
     */
    (withDefaultConfiguration: (app: App) => LambdaConfiguration) =>
        /**
         * Generates a Lambda configuration for a specific Lambda function.
         *
         * @param {string} lambdaId - The identifier of the specific Lambda function.
         * @returns {LambdaConfiguration} - The generated Lambda configuration for the specified Lambda function.
         */
        (lambdaId: string): LambdaConfiguration => {
            const defaultConfiguration = withDefaultConfiguration(app)
            const lambda = Optional(app.node.tryGetContext('lambda')).map(_ => _[lambdaId]).orElse({})
            return {
                runtime: Runtime[lambda.runtime] ?? defaultConfiguration.runtime,
                tracing: Tracing[lambda.tracing] ?? defaultConfiguration.tracing,
                timeout: lambda.timeout ? Duration.seconds(lambda.timeout) : defaultConfiguration.timeout,
                memorySize: lambda.memorySize ? Number(lambda.memorySize) : defaultConfiguration.memorySize,
                reservedConcurrentExecutions: lambda.reservedConcurrentExecutions ? Number(lambda.reservedConcurrentExecutions) : undefined,
                architecture: Architecture[lambda.architecture] ?? defaultConfiguration.architecture,
            }
        }

/**
 * Generates a scaling configuration for a specific Lambda function based on the provided CDK app context.
 *
 * @param {App} app - The CDK app instance.
 * @returns A curried function to generate scaling configurations for specific Lambda functions.
 */
export const withScalingConfiguration = (app: App) =>
    /**
     * Generates a scaling configuration for a specific Lambda function.
     *
     * @param {string} lambdaId - The identifier of the specific Lambda function.
     * @returns {Optional<ScalingConfiguration>} - The generated scaling configuration for the specified Lambda function.
     */
    (lambdaId: string): Optional<ScalingConfiguration> => {
        const maybeConfiguration = Optional(app.node.tryGetContext('lambda'))
            .map(_ => _[lambdaId])
            .map(lambdaConfiguration => {
                return Optional(lambdaConfiguration.scaling).map<ScalingConfiguration>(scalingConfiguration => {
                    const keys = Object.keys(scalingConfiguration)
                    if (keys.includes('minCapacity') && keys.includes('maxCapacity') && keys.includes('utilizationTarget')) {
                        return {
                            minCapacity: scalingConfiguration.minCapacity as number,
                            maxCapacity: scalingConfiguration.maxCapacity as number,
                            utilizationTarget: scalingConfiguration.utilizationTarget as number,
                        }
                    }

                    throw new Error('Incorrect lambda scaling configuration')
                })
            })

        if (maybeConfiguration.getError()) {
            maybeConfiguration.get()
        }

        return maybeConfiguration
    }

/**
 * Generates a Lambda function with an optional scaling configuration based on the provided CDK app context.
 *
 * @param {Stack} stack - The CDK stack instance.
 * @returns A curried function to generate Lambda functions with optional scaling.
 */
export const withLambda = (stack: Stack) =>
    /**
     * Generates a Lambda function with optional scaling configuration.
     *
     * @param {Function} withScalingConfiguration - A function to get scaling configuration.
     * @returns A curried function to generate Lambda functions with optional scaling.
     */
    (withScalingConfiguration: (lambdaId: string) => Optional<ScalingConfiguration>) =>
        /**
         * Generates a Lambda function with optional scaling configuration.
         *
         * @param {Function} withLambdaConfiguration - A function to get Lambda configuration.
         * @returns A curried function to generate Lambda functions with optional scaling.
         */
        (withLambdaConfiguration: (lambdaId: string) => LambdaConfiguration) =>
            /**
             * Generates a Lambda function with optional scaling configuration.
             *
             * @param {LambdaInput} input - The input data for the Lambda function.
             * @param {Partial<FunctionProps>} configuration - Additional configuration for the Lambda function.
             * @returns {Alias} - The alias for the Lambda function.
             */
            (input: LambdaInput, configuration?: Partial<FunctionProps>): IFunction => {
                const lambdaFunction = new Function(stack, input.functionName, {
                    ...withLambdaConfiguration(input.functionName),
                    functionName: input.functionName,
                    code: input.code,
                    handler: input.handler,
                    environment: input.environment,
                    ...Optional(configuration).orElse({}),
                })

                const alias = new Alias(stack, `${input.functionName}-Alias`, {
                    aliasName: DEFAULT_ALIAS_NAME,
                    version: lambdaFunction.latestVersion,
                })

                const maybeScalingConfiguration = withScalingConfiguration(input.functionName)
                maybeScalingConfiguration.map(configuration => {
                    const as = alias.addAutoScaling({
                        maxCapacity: configuration.maxCapacity,
                        minCapacity: configuration.minCapacity,
                    })

                    as.scaleOnUtilization({
                        utilizationTarget: configuration.utilizationTarget,
                    })

                    return true
                })

                return alias
            }

/**
 * Represents the input data required to import a Lambda function.
 */
export interface LambdaImportInput {
    readonly arn: string
    readonly id: string
}

/**
 * Imports a Lambda function based on the provided ARN and ID.
 *
 * @param {Stack} stack - The CDK stack instance.
 * @returns A function to import Lambda functions.
 */
export const importFunction = (stack: Stack) =>
    /**
     * Imports a Lambda function.
     *
     * @param {LambdaImportInput} input - The input data for importing the Lambda function.
     * @param {Partial<FunctionAttributes>} attributes - Additional attributes for the imported Lambda function.
     * @returns {IFunction} - The imported Lambda function.
     */
    (input: LambdaImportInput, attributes?: Partial<FunctionAttributes>): IFunction => {
        const fn = Function.fromFunctionAttributes(stack, input.id, {
            functionArn: input.arn,
            sameEnvironment: true,
            ...Optional(attributes).orElse({}),
        })

        return fn
    }

/**
 * Generates the ARN (Amazon Resource Name) of a Lambda function from its name and optional alias.
 */
export const functionArnFromName = (account: StackAccount) =>
    /**
     * @param fnName - The name of the Lambda function.
     * @param alias - Optional alias for the function.
     * @returns The ARN of the Lambda function.
     */
    (fnName: string, alias?: string): string => {
        return `arn:aws:lambda:${account.region}:${account.accountId}:function:${fnName}${Optional(alias).map(_ => `:${_}`).orElse('')}`
    }

/**
 * Generates the ARN (Amazon Resource Name) of a Lambda function from its name and the default alias.
 */
export const functionArnFromNameAndDefaultAlias = (account: StackAccount) =>
    /**
     * @param fnName - The name of the Lambda function.
     * @returns The ARN of the Lambda function with the default alias.
     */
    (fnName: string): string => {
        return functionArnFromName(account)(fnName, DEFAULT_ALIAS_NAME)
    }

/**
 * Creates a function to import a Lambda function based on its partial identifier and attributes.
 * @param functionArn - Function to generate the ARN of a Lambda function from its name.
 * @returns A function to import Lambda functions.
 */
export const importFunctionByPartialId = (functionArn: (fnName: string) => string) =>
    /**
     * Creates a function to import a Lambda function based on its partial identifier and attributes.
     * @param withId - Function to generate a full identifier based on a partial identifier.
     * @returns A function to import Lambda functions.
     */
    (withId: (identifier: string) => string) =>
        /**
         * Creates a function to import a Lambda function based on its partial identifier and attributes.
         * @param importFunction - Function to import the Lambda function.
         * @returns A function to import Lambda functions.
         */
        (importFunction: (input: LambdaImportInput, attributes?: Partial<FunctionAttributes>) => IFunction) =>
            /**
             * Creates a function to import a Lambda function based on its partial identifier and attributes.
             * @param id - Partial identifier of the Lambda function.
             * @param attributes - Optional attributes for the imported function.
             * @returns The imported Lambda function.
             */
            (id: string, attributes?: Partial<FunctionAttributes>) => {
                return importFunction({
                    id: withId(id),
                    arn: functionArn(withId(id)),
                }, attributes)
            }
