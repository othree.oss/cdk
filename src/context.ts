import {Optional, Try, TryAsync} from '@othree.io/optional'
import {CloudFormationClient, ListExportsCommand} from '@aws-sdk/client-cloudformation'
import {App, CfnOutput, Stack} from 'aws-cdk-lib'

/**
 * @module context
 */

/**
 * Loads CDK context values and validates them against constraints.
 *
 * @param validate - The validation function for the context.
 * @returns A function to load the cdk context.
 */
export const loadCdkContext = <Context extends Object>(validate: (constraints: any) => (input: Context) => Optional<Context>) =>
    /**
     * Load CDK context values and validate against constraints.
     *
     * @param app - The CDK app.
     * @param constraints - The constraints to apply to the context.
     * @returns {Optional<Context>}An optional context object that satisfies the constraints.
     */
    (app: App, constraints: Record<keyof Context, any>): Optional<Context> => {
        return Try(() => {
            const context = Object
                .keys(constraints)
                .reduce((context, key) => {
                    return Optional(app.node.tryGetContext(key))
                        .map(_ => ({
                            ...context,
                            [key]: _
                        }))
                        .orElse(context)
                }, {} as Context)

            return validate(constraints)(context).get()
        })
    }
