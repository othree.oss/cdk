import {Optional} from '@othree.io/optional'
import {Stack} from 'aws-cdk-lib'
import {Topic, TopicProps, SubscriptionFilter, ITopic} from 'aws-cdk-lib/aws-sns'
import {IQueue} from 'aws-cdk-lib/aws-sqs'
import {SqsSubscription, SqsSubscriptionProps} from 'aws-cdk-lib/aws-sns-subscriptions'
import {StackAccount} from './core'

/**
 * The sns module provides utility functions for working with Amazon SNS topics and subscriptions.
 * @module sns
 */

/**
 * Represents the health check event type.
 * @constant {string} HealthCheckEventType
 */
export const HealthCheckEventType = '$$HealthCheckRequested'
export const HealthCheckBC = '$$HealthCheckBC'

/**
 * Represents a subscription filter policy.
 * @typedef {Object} SubscriptionFilterPolicy
 */
export type SubscriptionFilterPolicy = { [key: string]: SubscriptionFilter }

/**
 * Represents the input for a topic.
 * @interface
 */
export interface TopicInput {
    /**
     * The unique identifier for the topic.
     * @readonly
     */
    readonly id: string
    /**
     * An optional display name for the topic.
     * @readonly
     */
    readonly displayName?: string
}

/**
 * Represents input for a Chisel filter policy.
 * @interface
 */
export interface ChiselFilterPolicyInput {
    /**
     * An array of Business Categories (BCs) used for filtering.
     * @readonly
     */
    readonly bcs: Array<string>

    /**
     * An optional array of event types used for additional filtering.
     * @readonly
     */
    readonly eventTypes?: Array<string>
}

/**
 * Represents input for an SQS subscription configuration.
 * @interface
 */
export interface SqsSubscriptionInput {
    /**
     * The topic to which the subscription is made.
     * @readonly
     */
    readonly topic: ITopic

    /**
     * The queue to which messages will be delivered.
     * @readonly
     */
    readonly queue: IQueue

    /**
     * An optional dead-letter queue (DLQ) for handling failed messages.
     * @readonly
     */
    readonly dlq?: IQueue
}

/**
 * Creates a function that configures and returns an instance of a topic.
 *
 * @param {Partial<TopicProps>} defaultConfiguration - Default configuration for the topic.
 * @returns A function that configures and returns an ITopic instance.
 * @example
 * // Define default and specific configurations
 * const defaultTopicConfig = {
 *   fifo: true
 * };
 *
 * const specificTopicConfig = {
 *   contentBasedDeduplication: true
 * };
 *
 * // Create the withTopic function using default configuration
 * const createTopic = withTopic(defaultTopicConfig);
 *
 * // Use the created function to configure and create topics
 * const topicInput = {
 *     id: 'my-topic',
 *     displayName: 'My Topic Display Name'
 * };
 *
 * const configuredTopic = createTopic(stack)(topicInput, specificTopicConfig);
 *
 * // Now you have a configured topic instance (configuredTopic) that you can work with
 */
export const withTopic = (defaultConfiguration?: Partial<TopicProps>) =>
    /**
     * Configures and returns a Topic instance within a specified stack.
     *
     * @param {Stack} stack - The stack in which the topic will be created.
     * @returns A function to configure and return a Topic instance.
     */
    (stack: Stack) =>
        /**
         * Configures and returns a Topic instance.
         *
         * @param {TopicInput} input - The input configuration for the topic.
         * @param {Partial<TopicProps>=} configuration - Additional optional configuration for the topic.
         * @returns {ITopic} - The configured Topic instance.
         */
        (input: TopicInput, configuration?: Partial<TopicProps>): ITopic => {
            const topic = new Topic(stack, input.id, {
                ...Optional(defaultConfiguration).orElse({}),
                topicName: input.id,
                displayName: input.displayName,
                ...Optional(configuration).orElse({})
            })

            return topic
        }

/**
 * Creates a SubscriptionFilterPolicy based on the provided ChiselFilterPolicyInput.
 *
 * @param {ChiselFilterPolicyInput} input - The input configuration for the Chisel filter policy.
 * @returns {SubscriptionFilterPolicy} - The configured SubscriptionFilterPolicy instance.
 *
 * @example
 * const filterPolicyInput = {
 *     bcs: ['bc1', 'bc2'],
 *     eventTypes: ['event1', 'event2']
 * };
 *
 * const filterPolicy = withChiselFilterPolicy(filterPolicyInput);
 *
 * // Now you have a configured filter policy instance (filterPolicy) that you can use
 */
export const withChiselFilterPolicy = (input: ChiselFilterPolicyInput): SubscriptionFilterPolicy => ({
    bc: SubscriptionFilter.stringFilter({
        allowlist: [...input.bcs, HealthCheckBC]
    }),
    ...(input.eventTypes && {
        eventType: SubscriptionFilter.stringFilter({
            allowlist: [...input.eventTypes, HealthCheckEventType]
        })
    })
})

/**
 * Creates a function to add an SQS subscription to a topic with optional filter policy.
 *
 * @param {SqsSubscriptionProps} configuration - Configuration for the SQS subscription.
 * @returns A function that adds an SQS subscription to a topic.
 *
 * @example
 * const sqsSubscriptionConfig = {
 *     deadLetterQueue: deadLetterQueue
 * };
 *
 * const addSqsSubscription = withSqsSubscription(sqsSubscriptionConfig);
 *
 * const filterPolicy = {
 *     properties: SubscriptionFilter.stringFilter({
 *         allowlist: ['all-props']
 *     })
 * };
 *
 * const subscriptionInput = {
 *     topic: myTopic,
 *     queue: myQueue,
 *     dlq: myDeadLetterQueue
 * };
 *
 * const subscriptionAdded = addSqsSubscription(filterPolicy)(subscriptionInput);
 *
 * // The subscription has been added (subscriptionAdded === true)
 */
export const withSqsSubscription = (configuration?: SqsSubscriptionProps) =>
    /**
     * Adds an SQS subscription to a topic with optional filter policy.
     *
     * @param {SubscriptionFilterPolicy} filterPolicy - The filter policy for the subscription.
     * @returns A function that adds an SQS subscription to a topic.
     */
    (filterPolicy?: SubscriptionFilterPolicy) =>
        /**
         * Adds an SQS subscription to a topic.
         *
         * @param {SqsSubscriptionInput} input - The input configuration for the subscription.
         * @returns {boolean} - Indicates whether the subscription was successfully added.
         */
        (input: SqsSubscriptionInput): boolean => {
            input.topic.addSubscription(new SqsSubscription(
                input.queue,
                {
                    deadLetterQueue: input.dlq,
                    ...Optional(filterPolicy).map(_ => ({filterPolicy: _})).orElse({} as any),
                    ...Optional(configuration).orElse({})
                }
            ))

            return true
        }

/**
 * Creates a function to generate an SNS topic ARN from an account and topic name.
 *
 * @param {StackAccount} account - The account details containing region and account ID.
 * @returns A function that generates an SNS topic ARN.
 *
 * @example
 * const myAccount = {
 *     region: 'us-east-1',
 *     accountId: '123456789012'
 * };
 *
 * const getTopicArn = topicArnFromName(myAccount);
 *
 * const topicName = 'my-topic';
 * const topicArn = getTopicArn(topicName);
 *
 * // topicArn will be "arn:aws:sns:us-east-1:123456789012:my-topic"
 */
export const topicArnFromName = (account: StackAccount) =>
    /**
     * Generates an SNS topic ARN from a topic name.
     *
     * @param {string} topicName - The name of the topic.
     * @returns {string} - The generated SNS topic ARN.
     */
    (topicName: string): string => {
        return `arn:aws:sns:${account.region}:${account.accountId}:${topicName}`
    }
