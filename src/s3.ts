import {Stack} from 'aws-cdk-lib'
import {IBucket, Bucket, BucketProps} from 'aws-cdk-lib/aws-s3'
import {BucketDeploymentProps, BucketDeployment, Source} from 'aws-cdk-lib/aws-s3-deployment'
import {Optional, TryAsync} from '@othree.io/optional'
import {Stats} from 'fs'
import {Hash} from 'crypto'

/**
 * @module s3
 */

/**
 * Represents input configuration for an S3 bucket.
 * @interface
 */
export interface BucketInput {
    readonly name: string
}

/**
 * Represents input configuration for deploying assets to an S3 bucket.
 * @interface
 */
export interface BucketDeploymentInput {
    readonly name: string
    readonly assetsPath: string
    readonly bucket: IBucket
}

/**
 * Represents output information from a bucket deployment.
 * @interface
 */
export interface BucketDeploymentOutput {
    readonly deploymentHash: string
}

/**
 * Creates a function that configures and returns an S3 bucket instance.
 *
 * @param {Stack} stack - The AWS CloudFormation stack to which the resources will be added.
 * @returns A curried function that returns an S3 bucket instance.
 */
export const withBucket = (stack: Stack) =>
    /**
     * Creates a function that configures and returns an S3 bucket instance.
     *
     * @param withId - Function to generate a full identifier based on a partial identifier.
     * @returns A curried function that returns an S3 bucket instance.
     */
    (withId: (identifier: string) => string) =>
        /**
         * Creates the S3 bucket instance.
         *
         * @param {BucketInput} input - Input settings for the S3 bucket.
         * @param {Partial<BucketProps>} [configuration] - Optional configuration for the bucket.
         * @returns {IBucket} The configured S3 bucket instance.
         */
        (input: BucketInput, configuration?: Partial<BucketProps>): IBucket => {
            return new Bucket(stack, withId(input.name), {
                bucketName: withId(input.name).toLowerCase(),
                ...Optional(configuration).orElse({})
            })
        }

/**
 * Creates a function that configures and returns a bucket deployment instance for local assets.
 *
 * @param {Stack} stack - The AWS CloudFormation stack to which the resources will be added.
 * @returns A curried function that returns a bucket deployment instance.
 */
export const withLocalAssetsBucketDeployment = (stack: Stack) =>
    /**
     * Creates a function that configures and returns a bucket deployment instance for local assets.
     *
     * @param withId - Function to generate a full identifier based on a partial identifier.
     * @returns A curried function that returns a bucket deployment instance.
     */
    (withId: (identifier: string) => string) =>
        /**
         * Configures the bucket deployment instance for local assets.
         *
         * @param {BucketDeploymentInput} input - Input settings for the bucket deployment.
         * @param {Partial<BucketDeploymentProps>} [configuration] - Optional configuration for the bucket deployment.
         * @returns {BucketDeployment} The configured bucket deployment instance.
         */
        (input: BucketDeploymentInput, configuration?: Partial<BucketDeploymentProps>): BucketDeployment => {
            return new BucketDeployment(stack, withId(input.name), {
                sources: [Source.asset(input.assetsPath)],
                destinationBucket: input.bucket,
                ...Optional(configuration).orElse({})
            })
        }

/**
 * Creates a function that configures and returns a versioned bucket deployment instance for local assets.
 *
 * @param {Function} readdir - A function to read directory contents asynchronously.
 * @returns A curried function that returns a promise with the bucket deployment output.
 */
export const withLocalAssetsVersionedBucketDeployment = (readdir: (dir: string) => Promise<Array<string>>) =>
    /**
     * Creates a function that configures and returns a versioned bucket deployment instance for local assets.
     *
     * @param {Function} stat - A function to retrieve file or directory stats asynchronously.
     * @returns A curried function that returns a promise with the bucket deployment output.
     */
    (stat: (dir: string) => Promise<Stats>) =>
        /**
         * Creates a function that configures and returns a versioned bucket deployment instance for local assets.
         *
         * @param {Function} readFile - A function to read file content asynchronously.
         * @returns A curried function that returns a promise with the bucket deployment output.
         */
        (readFile: (dir: string) => Promise<Buffer>) =>
            /**
             * Creates a function that configures and returns a versioned bucket deployment instance for local assets.
             *
             * @param {Function} createHash - A function to create a hash instance.
             * @returns A curried function that returns a promise with the bucket deployment output.
             */
            (createHash: (algorithm: string) => Hash) =>
                /**
                 * Creates a function that configures and returns a versioned bucket deployment instance for local assets.
                 *
                 * @param {Function} withLocalAssetsBucketDeployment - A function to configure a bucket deployment for local assets.
                 * @returns A curried function that returns a promise with the bucket deployment output.
                 */
                (withLocalAssetsBucketDeployment: (input: BucketDeploymentInput, configuration?: Partial<BucketDeploymentProps>) => BucketDeployment) =>
                    /**
                     * Configures and performs the versioned bucket deployment for local assets.
                     *
                     * @param {BucketDeploymentInput} input - Input settings for the bucket deployment.
                     * @param {Partial<BucketDeploymentProps>} [configuration] - Optional configuration for the bucket deployment.
                     * @returns {Promise<BucketDeploymentOutput>} A promise containing the deployment output.
                     */
                    async (input: BucketDeploymentInput, configuration?: Partial<BucketDeploymentProps>): Promise<BucketDeploymentOutput> => {
                        return TryAsync(async () => {
                            const deploymentHash = await readdir(input.assetsPath)
                                .then(assets => {
                                    return Promise.all(
                                        assets
                                            .map(asset => stat(`${input.assetsPath}/${asset}`)
                                                .then(stat => {
                                                    return !stat.isFile()
                                                        ? createHash('md5')
                                                            .update(Buffer.from(asset)).digest('hex')
                                                        : readFile(`${input.assetsPath}/${asset}`)
                                                            .then(fileBuffer => createHash('md5')
                                                                .update(fileBuffer).digest('hex')
                                                            )
                                                })
                                            )
                                    )
                                })
                                .then(filesHash => createHash('md5')
                                    .update(filesHash.join())
                                    .digest('hex')
                                )

                            withLocalAssetsBucketDeployment(input, configuration)

                            return {
                                deploymentHash: deploymentHash
                            }
                        }).then(_ => _.get())
                    }
