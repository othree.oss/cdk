import {CfnIdentityPool, CfnIdentityPoolProps, CfnIdentityPoolRoleAttachment} from 'aws-cdk-lib/aws-cognito'
import {Stack} from 'aws-cdk-lib'
import {FederatedPrincipal, ManagedPolicy, Role} from 'aws-cdk-lib/aws-iam'

/**
 * @module cognito
 */

/**
 * Represents the Cognito auth status.
 */
export enum CognitoAuth {
    AUTHENTICATED = 'authenticated',
    UNAUTHENTICATED = 'unauthenticated'
}

/**
 * Represents a collection of identity roles.
 */
export interface IdentityPoolRoles {
    readonly unauthenticatedRoleArn: string
    readonly authenticatedRoleArn: string
    readonly authenticatedAdminRoleArn: string
    readonly authenticatedReadOnlyRoleArn: string
}

/**
 * Represents the input to create an identity pool.
 */
export interface IdentityPoolInput {
    readonly projectName: string
    readonly userPoolId: string
    readonly configuration?: Partial<CfnIdentityPoolProps>
}

/**
 * Represents an identity pool id.
 */
export interface IdentityPoolOutput extends IdentityPoolRoles {
    readonly identityPoolId: string
}

/**
 * Represents the configuration to create an Okta integration.
 */
export interface OktaConfigurationInput {
    readonly projectName: string
    readonly userPoolId: string
}

/**
 * Represents the Okta integration output.
 */
export interface OktaConfigurationOutput extends IdentityPoolRoles {
    readonly identityPoolId: string
    readonly userPoolId: string
}

/**
 * Generates a Cognito role for an Identity Pool based on the provided inputs.
 *
 * @param {Stack} stack - The AWS CDK stack.
 * @returns A curried function that generates a Cognito role.
 */
export const withCognitoRole = (stack: Stack) =>
    /**
     * Generates a Cognito role for an Identity Pool based on the provided withId function, role name, identity pool ID, Cognito authentication type, and optional prefix name.
     *
     * @param {Function} withId - A function that generates unique identifiers.
     * @returns A curried function that generates a Cognito role.
     */
    (withId: (identifier: string) => string) =>
        /**
         * Generates a Cognito role for an Identity Pool based on the provided role name, identity pool ID, Cognito authentication type, and optional prefix name.
         *
         * @param {string} roleName - The name of the role.
         * @param {string} identityPoolId - The ID of the Identity Pool.
         * @param {CognitoAuth} cognitoAuth - The Cognito authentication type.
         * @param {string} [prefixName] - An optional prefix for the role name.
         * @returns {string} The ARN of the generated Cognito role.
         */
        (roleName: string, identityPoolId: string, cognitoAuth: CognitoAuth, prefixName?: string): string => {
            const role = new Role(stack, withId(roleName), {
                roleName: (prefixName) ? `${prefixName}${withId(roleName)}` : withId(roleName),
                assumedBy: new FederatedPrincipal(
                    'cognito-identity.amazonaws.com',
                    {
                        StringEquals: {
                            'cognito-identity.amazonaws.com:aud': identityPoolId,
                        },
                        'ForAnyValue:StringLike': {
                            'cognito-identity.amazonaws.com:amr': cognitoAuth.valueOf(),
                        },
                    },
                    'sts:AssumeRoleWithWebIdentity',
                ),
                managedPolicies: [ManagedPolicy.fromAwsManagedPolicyName('AmazonOpenSearchServiceCognitoAccess')],
            })

            return role.roleArn
        }

/**
 * Generates an AWS Cognito Identity Pool based on the provided inputs.
 *
 * @param {Stack} stack - The AWS CDK stack.
 * @returns A curried function that generates an Identity Pool.
 */
export const withIdentityPool = (stack: Stack) =>
    /**
     * Generates an AWS Cognito Identity Pool based on the provided withId function and withCognitoRole function.
     *
     * @param {Function} withId - A function that generates unique identifiers.
     * @returns A curried function that generates an Identity Pool.
     */
    (withId: (identifier: string) => string) =>
        /**
         * Generates an AWS Cognito Identity Pool and associated roles based on the provided withCognitoRole function and input configuration.
         *
         * @param {Function} withCognitoRole - A function that generates Cognito roles.
         * @returns A curried function that generates an Identity Pool.
         */
        (withCognitoRole: (roleName: string, identityPoolId: string, cognitoAuth: CognitoAuth, prefixName?: string) => string) =>
            /**
             * Generates an AWS Cognito Identity Pool and associated roles based on the provided input configuration.
             *
             * @param {IdentityPoolInput} input - The input configuration for the Identity Pool.
             * @returns {IdentityPoolOutput} The generated Identity Pool and associated roles.
             */
            (input: IdentityPoolInput): IdentityPoolOutput => {

                const identityPool = new CfnIdentityPool(stack, withId(`${input.projectName}-IdPool`), {
                    allowUnauthenticatedIdentities: false,
                    ...input.configuration,
                })

                const unauthenticatedRoleArn = withCognitoRole(`${input.projectName}-Role-UnAuth`, identityPool.ref, CognitoAuth.UNAUTHENTICATED)
                const authenticatedRoleArn = withCognitoRole(`${input.projectName}-Role-Auth`, identityPool.ref, CognitoAuth.AUTHENTICATED)
                const authenticatedAdminRoleArn = withCognitoRole(`${input.projectName}-Role-AuthAdmin`, identityPool.ref, CognitoAuth.AUTHENTICATED, '0')
                const authenticatedReadOnlyRoleArn = withCognitoRole(`${input.projectName}-Role-AuthReadOnly`, identityPool.ref, CognitoAuth.AUTHENTICATED, '0')

                new CfnIdentityPoolRoleAttachment(stack, withId(`${input.projectName}-IdPool-Roles`), {
                    identityPoolId: identityPool.ref,
                    roles: {
                        unauthenticated: unauthenticatedRoleArn,
                        authenticated: authenticatedRoleArn,
                    },
                })

                return {
                    identityPoolId: identityPool.ref,
                    unauthenticatedRoleArn: unauthenticatedRoleArn,
                    authenticatedRoleArn: authenticatedRoleArn,
                    authenticatedAdminRoleArn: authenticatedAdminRoleArn,
                    authenticatedReadOnlyRoleArn: authenticatedReadOnlyRoleArn,
                }

            }

/**
 * Generates an Okta configuration based on the provided Identity Pool configuration.
 *
 * @param {Function} withIdentityPool - A function that generates an Identity Pool configuration.
 * @returns A function that generates Okta configuration.
 */
export const withOktaConfiguration = (withIdentityPool: (input: IdentityPoolInput) => IdentityPoolOutput) =>
    /**
     * Generates an Okta configuration based on the provided input.
     *
     * @param {OktaConfigurationInput} input - The input configuration for Okta.
     * @returns {OktaConfigurationOutput} The generated Okta configuration.
     */
    (input: OktaConfigurationInput): OktaConfigurationOutput => {

        const identityPoolOutput = withIdentityPool({
            projectName: input.projectName,
            userPoolId: input.userPoolId,
        })

        return {
            identityPoolId: identityPoolOutput.identityPoolId,
            userPoolId: input.userPoolId,
            unauthenticatedRoleArn: identityPoolOutput.unauthenticatedRoleArn,
            authenticatedRoleArn: identityPoolOutput.authenticatedRoleArn,
            authenticatedAdminRoleArn: identityPoolOutput.authenticatedAdminRoleArn,
            authenticatedReadOnlyRoleArn: identityPoolOutput.authenticatedReadOnlyRoleArn,
        }
    }
