import {App, DefaultStackSynthesizer, Stack} from 'aws-cdk-lib'
import {IpAddresses, SubnetType, Vpc} from 'aws-cdk-lib/aws-ec2'
import {vpc} from '../src'
import {Template    } from 'aws-cdk-lib/assertions'

describe('vpc', () => {
    it('should create a vpc peering connection', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const withId = jest.fn().mockImplementation((id: string) => `Test-${id}-V1`)

        const fromVpc = new Vpc(stack, 'FromVpc', {
            maxAzs: 1,
            subnetConfiguration: [{
                cidrMask: 28,
                name: 'ingress',
                subnetType: SubnetType.PUBLIC,
            }],
            ipAddresses: IpAddresses.cidr('10.0.0.0/24')
        })

        const toVpc = new Vpc(stack, 'ToVpc', {
            subnetConfiguration: [{
                cidrMask: 28,
                name: 'ingress',
                subnetType: SubnetType.PUBLIC,
            }, {
                cidrMask: 28,
                name: 'compute',
                subnetType: SubnetType.PRIVATE_WITH_EGRESS,
            }, {
                cidrMask: 28,
                name: 'rds',
                subnetType: SubnetType.PRIVATE_ISOLATED,
            }],
            ipAddresses: IpAddresses.cidr('10.0.1.0/24')
        })

        vpc.withVpcPeering(stack)(withId)({
            fromVpcPeering: {
                vpc: fromVpc,
                subnetSelection: {
                    subnetType: SubnetType.PUBLIC
                }
            },
            toVpcPeering: {
                vpc: toVpc,
                subnetSelection: {
                    subnetType: SubnetType.PRIVATE_ISOLATED
                }
            }
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})
