import {stack, sns} from '../src'
import {App} from 'aws-cdk-lib'
import {Template} from 'aws-cdk-lib/assertions'
import {loadProjectMetadata, ProjectMetadata} from '../src/stack'

describe('stack', () => {
    delete process.env.CI_COMMIT_REF_NAME
    process.env.USER = 'cdk-test'
    it('should create a stack', () => {
        const app = new App({
            context: {
                metadata: {
                    project: 'CDK',
                    owner: 'cocoa'
                },
                env: 'Dev',
                volatile: 'true'
            }
        })

        const newStack = stack.withStack(stack.withDefaultConfiguration(loadProjectMetadata(app), 'Stack'))(app)({
            id: 'Test-Stack'
        }, {
            tags: [{
                key: 'ExtraTagKey',
                value: 'ExtraTagValue'
            }]
        })

        sns.withTopic({
            fifo: true,
            displayName: 'Display-Name-Test-Topic'
        })(newStack)({
            id: 'Test-Topic'
        }, {
            contentBasedDeduplication: true
        })

        const template = Template.fromStack(newStack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a stack using the pipeline branch as the version tag', () => {
        process.env.CI_COMMIT_REF_NAME = 'feature/some-new-feature'
        const app = new App({
            context: {
                metadata: {
                    project: 'CDK',
                    owner: 'cocoa'
                },
                env: 'Dev',
                volatile: 'true'
            }
        })

        const newStack = stack.withStack(stack.withDefaultConfiguration(loadProjectMetadata(app), 'Stack'))(app)({
            id: 'Test-Stack'
        }, {
            tags: [{
                key: 'ExtraTagKey',
                value: 'ExtraTagValue'
            }]
        })

        sns.withTopic({
            fifo: true,
            displayName: 'Display-Name-Test-Topic'
        })(newStack)({
            id: 'Test-Topic'
        }, {
            contentBasedDeduplication: true
        })

        const template = Template.fromStack(newStack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})
