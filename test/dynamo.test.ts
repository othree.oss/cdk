import {AttributeType} from 'aws-cdk-lib/aws-dynamodb'
import {Template} from 'aws-cdk-lib/assertions'
import {Stack, App, DefaultStackSynthesizer} from 'aws-cdk-lib'
import {dynamo} from '../src'
import {core} from '../src'

describe('dynamo', () => {
    it('should create a dynamo table', () => {
        const app = new App({
            context: {
                env: 'Dev',
                volatile: 'true'
            }
        })

        const context = core.appContext(app)
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        dynamo.withTable(dynamo.withDefaultConfiguration(context))(stack)({
            id: 'Table-Test',
            partitionKey: {name: 'id', type: AttributeType.STRING},
            sortKey: {name: 'name', type: AttributeType.STRING}
        }, {
            pointInTimeRecovery: true
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a dynamo table using chisel input', () => {
        const app = new App({
            context: {
                env: 'Dev',
                volatile: 'false'
            }
        })

        const context = core.appContext(app)
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        dynamo.withTable(dynamo.withDefaultConfiguration(context))(stack)
        (dynamo.withChiselEventsTableInput('Chisel-Table-Test'), {
            pointInTimeRecovery: true
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})

