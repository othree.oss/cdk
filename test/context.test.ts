import {context} from '../src'
import {Optional} from '@othree.io/optional'
import {App} from 'aws-cdk-lib'

describe('loadCdkContext', () => {
    it('should load the cdk context', () => {
        const contextResult = {
            variable1: 'P00L-Dev'
        }

        const constraints = {
            variable1: {
                presence: true
            }
        }

        const app = new App({
            context: contextResult
        })

        const validateFn = jest.fn().mockReturnValue(Optional(contextResult))
        const validate = jest.fn().mockReturnValue(validateFn)

        const maybeContext = context.loadCdkContext(validate)(app, constraints)

        expect(maybeContext.isPresent).toBeTruthy()
        expect(maybeContext.get()).toStrictEqual(contextResult)

        expect(validate).toBeCalledTimes(1)
        expect(validate).toBeCalledWith(constraints)
        expect(validateFn).toBeCalledTimes(1)
        expect(validateFn).toBeCalledWith(contextResult)
    })
})

