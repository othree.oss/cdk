import {Template} from 'aws-cdk-lib/assertions'
import {sns} from '../src'
import {Stack, App} from 'aws-cdk-lib'
import {Queue} from 'aws-cdk-lib/aws-sqs'
import {SubscriptionFilter} from 'aws-cdk-lib/aws-sns'
import {StackAccount} from '../src/core'
import {DefaultStackSynthesizer} from 'aws-cdk-lib'

describe('sns', () => {
    describe('withTopic', () => {
        it('should create a topic', () => {
            const app = new App()
            const stack = new Stack(app, 'TestStack', {
                synthesizer: new DefaultStackSynthesizer({
                    generateBootstrapVersionRule: false
                })
            })

            sns.withTopic({
                fifo: true,
                displayName: 'Display-Name-Test-Topic'
            })(stack)({
                id: 'Test-Topic'
            }, {
                contentBasedDeduplication: true
            })

            const template = Template.fromStack(stack)
            expect(template.toJSON()).toMatchSnapshot()
        })
    })

    describe('withChiselFilterPolicy', () => {
        it('should create a chisel filter policy', () => {
            const chiselFilterPolicy = sns.withChiselFilterPolicy({
                bcs: ['test-bc'],
                eventTypes: ['TestBCCreated']
            })

            expect(chiselFilterPolicy).toStrictEqual({
                bc: SubscriptionFilter.stringFilter({
                    allowlist: ['test-bc', '$$HealthCheckBC']
                }),
                eventType: SubscriptionFilter.stringFilter({
                    allowlist: ['TestBCCreated', '$$HealthCheckRequested']
                })
            })
        })
    })

    describe('withSqsSubscription', () => {
        it('should add a subscription to a topic', () => {
            const app = new App()
            const stack = new Stack(app, 'TestStack', {
                synthesizer: new DefaultStackSynthesizer({
                    generateBootstrapVersionRule: false
                })
            })
            const queue = new Queue(stack, 'Test-Queue')
            const topic = sns.withTopic()(stack)({
                id: 'Test-Topic'
            })
            sns.withSqsSubscription()()({
                topic: topic,
                queue: queue
            })

            const template = Template.fromStack(stack)
            expect(template.toJSON()).toMatchSnapshot()
        })

        it('should add a subscription to a topic using a filter policy', () => {
            const app = new App()
            const stack = new Stack(app, 'TestStack', {
                synthesizer: new DefaultStackSynthesizer({
                    generateBootstrapVersionRule: false
                })
            })
            const queue = new Queue(stack, 'Test-Queue')
            const topic = sns.withTopic()(stack)({
                id: 'Test-Topic-Filter-Policy'
            })
            sns.withSqsSubscription()({
                properties: SubscriptionFilter.stringFilter({
                    allowlist: ['all-props']
                })
            })({
                topic: topic,
                queue: queue
            })

            const template = Template.fromStack(stack)
            expect(template.toJSON()).toMatchSnapshot()
        })
    })

    describe('topicArnFromName', () => {
        it('should generate an arn from a topic name', () => {
            const stackAccount: StackAccount = {
                region: 'us-west-2',
                accountId: '1337'
            }

            const arn = sns.topicArnFromName(stackAccount)('Test-Topic-Dev-V1')

            expect(arn).toEqual('arn:aws:sns:us-west-2:1337:Test-Topic-Dev-V1')
        })
    })
})
