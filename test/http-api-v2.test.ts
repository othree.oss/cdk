import {
    composedHttpApi,
    HttpApiConfiguration, withAuthorizerConfiguration,
    withDefaultApiConfiguration,
    withHttpApi,
    withRoute
} from '../src/http-api-v2'
import {Cors} from 'aws-cdk-lib/aws-apigateway'
import {App, DefaultStackSynthesizer, Stack} from 'aws-cdk-lib'
import {Template} from 'aws-cdk-lib/assertions'
import {Code, Function, Runtime} from 'aws-cdk-lib/aws-lambda'
import {CorsHttpMethod, HttpMethod} from '@aws-cdk/aws-apigatewayv2-alpha'
import {HttpJwtAuthorizer} from '@aws-cdk/aws-apigatewayv2-authorizers-alpha'

describe('withDefaultApiConfiguration', () => {
    it('should return the default configuration', () => {
        expect(withDefaultApiConfiguration()).toMatchSnapshot()
    })
})

describe('withNewRestApi', () => {
    const configuration: HttpApiConfiguration = {
        corsPreflight: {
            allowCredentials: false,
            allowMethods: [CorsHttpMethod.GET],
            allowOrigins: Cors.ALL_ORIGINS,
            allowHeaders: Cors.DEFAULT_HEADERS
        }
    }

    it('should create a rest api with the specified resources', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const testFnRoot = new Function(stack, 'TestFnRoot', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index',
            runtime: Runtime.NODEJS_14_X
        })

        const testFn1 = new Function(stack, 'TestFn1', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index',
            runtime: Runtime.NODEJS_14_X
        })

        const testFn2 = new Function(stack, 'TestFn2', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index2',
            runtime: Runtime.NODEJS_14_X
        })

        const api = withHttpApi(configuration)(stack)({apiName: 'SuperApi'})

        composedHttpApi({})(api,
            withRoute('/', {method: HttpMethod.GET, fn: testFnRoot}),
            withRoute('/v1/orders/{orderId}/another/resource', {method: HttpMethod.GET, fn: testFn1}),
            withRoute('/v1/orders/{orderId}/items', {
                method: HttpMethod.PUT,
                fn: testFn2,
                options: {
                    authorizationScopes: ['scope1']
                }
            })
        )

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a rest api with the specified subdomain, authorization and resources', () => {
        const withDomainConfiguration: HttpApiConfiguration = {
            ...configuration,
            ...withAuthorizerConfiguration('DefaultAuthorizer', 'http://public.othree.com/', ['testApp']),
            domain: {
                domain: 'othree.dev',
                basePath: 'test'
            }
        }
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const testFn1 = new Function(stack, 'TestFn1', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index',
            runtime: Runtime.NODEJS_14_X
        })

        const testFn2 = new Function(stack, 'TestFn2', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index2',
            runtime: Runtime.NODEJS_14_X
        })

        const api = withHttpApi(withDomainConfiguration)(stack)({apiName: 'SuperApi'})

        const authorizer = new HttpJwtAuthorizer('TestJwtAuthorizer', 'http://public.othree.com/', {
            authorizerName: 'http://public.othree.com/',
            jwtAudience: ['testApp']
        })

        composedHttpApi({
            authorizer: authorizer
        })(api,
            withRoute('/v1/orders/{orderId}/another/resource', {method: HttpMethod.GET, fn: testFn1}),
            withRoute('/v1/orders/{orderId}/items', {
                method: HttpMethod.PUT,
                fn: testFn2,
                options: {
                    authorizationScopes: ['scope1']
                }
            })
        )

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})
