import {core, sqs} from '../src'
import {Template} from 'aws-cdk-lib/assertions'
import {Stack, App} from 'aws-cdk-lib/core'
import {StackAccount} from '../src/core'
import {DefaultStackSynthesizer} from 'aws-cdk-lib'

describe('sqs', () => {
    it('should create a queue with a destroy removal policy', () => {
        const app = new App({
            context: {
                env: 'Dev',
                volatile: 'true'
            }
        })

        const context = core.appContext(app)
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const testDLQ = sqs.withQueue(sqs.withDefaultConfiguration(context))(stack)({
            id: 'Test-DLQueue-Removal'
        })

        sqs.withQueue(sqs.withDefaultConfiguration(context))(stack)({
            id: 'Test-Queue-Removal',
            dlq: {
                queue: testDLQ,
                maxReceiveCount: 5
            }
        }, {
            maxMessageSizeBytes: 1024
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a queue with a retain removal policy', () => {
        const app = new App({
            context: {
                env: 'Dev',
                volatile: 'false'
            }
        })

        const context = core.appContext(app)
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const testDLQ = sqs.withQueue(sqs.withDefaultConfiguration(context))(stack)({
            id: 'Test-DLQueue-Retain'
        })

        sqs.withQueue(sqs.withDefaultConfiguration(context))(stack)({
            id: 'Test-Queue-Retain',
            dlq: {
                queue: testDLQ,
                maxReceiveCount: 5
            }
        }, {
            maxMessageSizeBytes: 1024
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    describe('queueArnFromName', () => {
        it('should generate an arn from a queue name', () => {
            const stackAccount: StackAccount = {
                region: 'us-west-2',
                accountId: '1337'
            }

            const arn = sqs.queueArnFromName(stackAccount)('Test-Queue-Dev-V1')

            expect(arn).toEqual('arn:aws:sqs:us-west-2:1337:Test-Queue-Dev-V1')
        })
    })
})
