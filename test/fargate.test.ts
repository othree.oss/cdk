import {withFargateService} from '../src/fargate'
import {App, DefaultStackSynthesizer, Stack} from 'aws-cdk-lib'
import {DockerImageAsset} from 'aws-cdk-lib/aws-ecr-assets'
import {Template} from 'aws-cdk-lib/assertions'
import {Effect, PolicyStatement} from 'aws-cdk-lib/aws-iam'

describe('withFargateService', () => {

    const withId = (id: string) => id

    it('should create a fargate service', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false,
            }),
        })

        const image = new DockerImageAsset(stack, 'TestImage', {
            directory: './test/mock-image',
        })

        withFargateService(withId)(stack)({
            cpu: 256,
            memoryLimitMiB: 512,
            publicIp: true,
        })({
            taskCount: 1,
            name: 'TestTask',
            image: image,
            policies: [new PolicyStatement({
                effect: Effect.ALLOW,
                actions: ['lambda:*'],
                resources: ['*'],
            })],
            environment: {},
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a fargate service without public ip', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false,
            }),
        })

        const image = new DockerImageAsset(stack, 'TestImage', {
            directory: './test/mock-image',
        })

        withFargateService(withId)(stack)({
            cpu: 256,
            memoryLimitMiB: 512,
            publicIp: true,
        })({
            taskCount: 1,
            name: 'TestTask',
            image: image,
            policies: [new PolicyStatement({
                effect: Effect.ALLOW,
                actions: ['lambda:*'],
                resources: ['*'],
            })],
            environment: {},
        }, {
            publicIp: false,
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})
