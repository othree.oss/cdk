import {App, DefaultStackSynthesizer, Stack} from 'aws-cdk-lib'
import {customResource} from '../src'
import {Template} from 'aws-cdk-lib/assertions'

describe('custom-resource', () => {

    it('should create a custom resource', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const withId = jest.fn().mockImplementation((id) => `Test-${id}-V1`)

        const resourceConfiguration = customResource.withDefaultCustomResourceConfiguration()

        const onCreateSdkCall = customResource.withLambdaSdkCall({
            payload: {input: '123'},
            functionArn: 'arn:aws:lambda:us-west-2:1337:function:FnNameCreate',
            hash: 'H@5h'
        })

        const onUpdateSdkCall = customResource.withLambdaSdkCall({
            payload: {input: '123'},
            functionArn: 'arn:aws:lambda:us-west-2:1337:function:FnNameUpdate',
            hash: 'H@5h'
        })

        const onDeleteSdkCall = customResource.withLambdaSdkCall({
            payload: {input: '123'},
            functionArn: 'arn:aws:lambda:us-west-2:1337:function:FnNameDelete',
            hash: 'H@5h'
        })

        customResource.withCustomResource(stack)(withId)(resourceConfiguration)(onCreateSdkCall)(onUpdateSdkCall)(onDeleteSdkCall)({
            name: 'Custom-Resource'
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a dynamo batch write custom resource', () => {

        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const withId = jest.fn().mockImplementation((id) => `Test-${id}-V1`)

        const resourceConfiguration = customResource.withDefaultCustomResourceConfiguration()

        const onCreateSdkCall = customResource.withDynamoBatchWriteCall({
            name: 'test-write-batch',
            items: [{input: '123'}, {input: '123'}, {input: '123'}],
            tableArn: 'arn:aws:dynamodb:us-west-2:1337:table/test-table',
            tableName: 'test-table'
        })

        customResource.withCustomResource(stack)(withId)(resourceConfiguration)(onCreateSdkCall)()()({
            name: 'Custom-Resource'
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a dynamo batch write custom resource with more than items limit', () => {

        const items = new Array(26).fill({input: '123'})

        expect( () => customResource.withDynamoBatchWriteCall({
            name: 'test-write-batch',
            items: items,
            tableArn: 'arn:aws:dynamodb:us-west-2:1337:table/test-table',
            tableName: 'test-table'
        })).toThrowError('Batch write items length should be 25 or less')

    })

})
