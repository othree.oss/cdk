import {lambda, rds} from '../src'
import {App, DefaultStackSynthesizer, Stack} from 'aws-cdk-lib'
import {IpAddresses, SubnetType, Vpc} from 'aws-cdk-lib/aws-ec2'
import {Template} from 'aws-cdk-lib/assertions'
import {AuroraCapacityUnit, Credentials} from 'aws-cdk-lib/aws-rds'

describe('rds', () => {
    it('should create a serverless rds cluster', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const withId = jest.fn().mockImplementation((id: string) => `Test-${id}-V1`)

        const clusterName = 'AwesomeCluster'

        const serverlessScalingConfiguration = rds.withServerlessScalingConfiguration(app)(`Test-${clusterName}-V1`)
        const serverlessRdsConfiguration = rds.withServerlessRdsConfiguration(serverlessScalingConfiguration)('main', 'testUser')
        const engineConfiguration = rds.withAuroraPostgresSql10EngineConfiguration(stack)(withId)
        const databaseCredentials = rds.withDatabaseCredentials(stack)('Test-Credentials', 'adminUser')

        const vpc = new Vpc(stack, 'RdsVpc', {
            subnetConfiguration: [{
                cidrMask: 28,
                name: 'ingress',
                subnetType: SubnetType.PUBLIC,
            }, {
                cidrMask: 28,
                name: 'compute',
                subnetType: SubnetType.PRIVATE_WITH_EGRESS,
            }, {
                cidrMask: 28,
                name: 'rds',
                subnetType: SubnetType.PRIVATE_ISOLATED,
            }],
            ipAddresses: IpAddresses.cidr('10.0.1.0/24')
        })

        rds.withServerlessRds(stack)(withId)(serverlessRdsConfiguration)(engineConfiguration)({
            clusterName: clusterName,
            selectedSubnets: vpc.selectSubnets({
                subnetType: SubnetType.PRIVATE_ISOLATED
            }),
            vpc: vpc,
            credentials: Credentials.fromSecret(databaseCredentials)
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create the default scaling configuration', () => {
        const app = new App()

        const scalingConfiguration = rds.withServerlessScalingConfiguration(app)('Cocoa-Test-Cluster-Dev')

        expect(scalingConfiguration).toStrictEqual({
            minCapacity: AuroraCapacityUnit.ACU_2,
            maxCapacity: AuroraCapacityUnit.ACU_4
        })
    })

    it('should create the scaling configuration using the context', () => {
        const app = new App({
            context: {
                'Cocoa-Test-Cluster-Dev': {
                    scaling: {
                        minCapacity: 8,
                        maxCapacity: 16,
                    }
                }
            }
        })

        const scalingConfiguration = rds.withServerlessScalingConfiguration(app)('Cocoa-Test-Cluster-Dev')

        expect(scalingConfiguration).toStrictEqual({
            minCapacity: AuroraCapacityUnit.ACU_8,
            maxCapacity: AuroraCapacityUnit.ACU_16
        })
    })

    it('should create the default scaling configuration using the context if the scaling property is not present for that cluster', () => {
        const app = new App({
            context: {
                'Cocoa-Test-Cluster-Dev': {}
            }
        })

        const scalingConfiguration = rds.withServerlessScalingConfiguration(app)('Cocoa-Test-Cluster-Dev')

        expect(scalingConfiguration).toStrictEqual({
            minCapacity: AuroraCapacityUnit.ACU_2,
            maxCapacity: AuroraCapacityUnit.ACU_4
        })
    })

    it('should try to create the scaling configuration and throw an exception when the configuration is not valid', () => {
        const app = new App({
            context: {
                'Cocoa-Test-Cluster-Dev': {
                    scaling: {
                        min: 8,
                        max: 16,
                    }
                }
            }
        })

        expect(() => rds.withServerlessScalingConfiguration(app)('Cocoa-Test-Cluster-Dev')).toThrow(new Error('Invalid scaling configuration'))
    })
})
