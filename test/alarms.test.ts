import {
    AlarmType, FunctionErrorAlarmInput, LambdaErrorAlarmInput, RestApiErrorAlarmInput,
    withAlarm,
    withAlarmId,
    withAlarmsContext, withFunctionCountAlarm, withFunctionHttpRequestErrorAlarm,
    withLambdaErrorsAlarm,
    withProjectionDLQAlarm, withRestApi4XXErrorsAlarm, withRestApi5XXErrorsAlarm, withSqsAlarm
} from '../src/alarms'
import {App, DefaultStackSynthesizer, Stack} from 'aws-cdk-lib'
import {AppContext} from '../src/core'
import {Metric, TreatMissingData} from 'aws-cdk-lib/aws-cloudwatch'
import {Template} from 'aws-cdk-lib/assertions'

describe('withAlarmsContext', () => {
    it('should return the alarms context', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const withId = (id: string) => id
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })

        expect(context.criticalAlarmTopic.topicArn).toEqual('arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn')
        expect(context.mildAlarmTopic.topicArn).toEqual('arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn')
    })
})

describe('withAlarmId', () => {
    it('should return the expected id', () => {
        const context: AppContext = {
            env: 'Dev',
            volatile: true,
            optimize: false,
            shareCommonDataStores: false
        }

        const alarmId = withAlarmId(context)('TestProject')('DLQ', 'Bad Alarm')

        expect(alarmId).toEqual('[TestProject-DLQ] Bad Alarm [Dev]')
    })
})

describe('withAlarm', () => {
    it('should return a critical alarm', () => {
        const withId = (id: string) => id
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })

        withAlarm(stack)(context)({
            alarmId: 'TheAlarm',
            alarmType: AlarmType.CRITICAL
        }, {
            metric: new Metric({
                namespace: 'Cocoa/Namespace',
                metricName: 'Count'
            }),
            alarmName: 'AwesomeAlarm',
            threshold: 1,
            evaluationPeriods: 1
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should return a mild alarm', () => {
        const withId = (id: string) => id
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })

        withAlarm(stack)(context)({
            alarmId: 'TheAlarm',
            alarmType: AlarmType.MILD
        }, {
            metric: new Metric({
                namespace: 'Cocoa/Namespace',
                metricName: 'Count'
            }),
            alarmName: 'AwesomeAlarm',
            threshold: 1,
            evaluationPeriods: 1
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})

describe('withSqsAlarm', () => {
    it('should create an alarm for the specified SQS', () => {
        const withId = (id: string) => id
        const withAlarmId = (alarmSource: string, alarmName: string) => `${alarmSource}-${alarmName}`

        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })
        withSqsAlarm(withId)(withAlarmId)(stack)(context)({
            id:'TestDLQ',
            alarmType: AlarmType.CRITICAL,
            alarmDescription: 'DLQ Alarm description',
            sqsArn: 'arn:aws:sqs:us-west-2:12345678890:TestDLQ',
            sqsThreshold: 1,
            sqsEvaluationPeriod: 1,
            sqsDatapointsToAlarm: 1
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})

describe('withProjectionDLQAlarm', () => {
    it('should create an alarm for the specified DLQ', () => {
        const withId = (id: string) => id
        const withAlarmId = (alarmSource: string, alarmName: string) => `${alarmSource}-${alarmName}`

        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })
        withProjectionDLQAlarm(withId)(withAlarmId)(stack)(context)('TestDLQ', 'arn:aws:sqs:us-west-2:12345678890:TestDLQ')

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})

describe('withLambdaErrorsAlarm', () => {

    it('should create an alarm for the specified lambda errors', () => {

        const withId = (id: string) => id
        const withAlarmId = (alarmSource: string, alarmName: string) => `${alarmSource}-${alarmName}`

        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })

        const input: LambdaErrorAlarmInput = {
            id: 'Cocoa-Test-Handler',
            alarmType: AlarmType.CRITICAL,
            lambdaArn: 'arn:aws:lambda:us-west-2:435692684201:function:Cocoa-Test-Handler-Dev',
            alarmDescription: 'Alarm Description Test',
        }

        withLambdaErrorsAlarm(withId)(withAlarmId)(stack)(context)(input)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create an alarm for the specified lambda errors and override props', () => {

        const withId = (id: string) => id
        const withAlarmId = (alarmSource: string, alarmName: string) => `${alarmSource}-${alarmName}`

        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })

        const input: LambdaErrorAlarmInput = {
            id: 'Cocoa-Test-Handler',
            alarmType: AlarmType.CRITICAL,
            lambdaArn: 'arn:aws:lambda:us-west-2:435692684201:function:Cocoa-Test-Handler-Dev',
            alarmDescription: 'Alarm Description Test',
            alarmProps: {
                evaluationPeriods: 2,
                datapointsToAlarm: 2
            }
        }

        withLambdaErrorsAlarm(withId)(withAlarmId)(stack)(context)(input)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

})

describe('withApiGatewayErrorsAlarm', () => {

    it('should create an alarm for 5XX errors', () => {

        const withId = (id: string) => id
        const withAlarmId = (alarmSource: string, alarmName: string) => `${alarmSource}-${alarmName}`

        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })

        const input: RestApiErrorAlarmInput = {
            id: 'Cocoa-Rest-Api-Test',
            alarmType: AlarmType.CRITICAL,
            apiId: 'jiygflgxzg',
            alarmDescription: 'Alarm description for Rest Api 5XX Errors',
            alarmThreshold: 100,
            alarmEvaluationPeriods: 1,
            alarmDatapoints: 1
        }

        withRestApi5XXErrorsAlarm(withId)(withAlarmId)(stack)(context)(input)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create an alarm for 5XX errors and override props', () => {

        const withId = (id: string) => id
        const withAlarmId = (alarmSource: string, alarmName: string) => `${alarmSource}-${alarmName}`

        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })

        const input: RestApiErrorAlarmInput = {
            id: 'Cocoa-Rest-Api-Test',
            alarmType: AlarmType.CRITICAL,
            apiId: 'jiygflgxzg',
            alarmDescription: 'Alarm description for Rest Api 5XX Errors',
            alarmThreshold: 100,
            alarmEvaluationPeriods: 1,
            alarmDatapoints: 1,
            alarmProps: {
                treatMissingData: TreatMissingData.IGNORE
            }
        }

        withRestApi5XXErrorsAlarm(withId)(withAlarmId)(stack)(context)(input)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create an alarm for 4XX errors', () => {

        const withId = (id: string) => id
        const withAlarmId = (alarmSource: string, alarmName: string) => `${alarmSource}-${alarmName}`

        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })

        const input: RestApiErrorAlarmInput = {
            id: 'Cocoa-Rest-Api-Test',
            alarmType: AlarmType.CRITICAL,
            apiId: 'jiygflgxzg',
            alarmDescription: 'Alarm description for Rest Api 4XX Errors',
            alarmThreshold: 100,
            alarmEvaluationPeriods: 1,
            alarmDatapoints: 1
        }

        withRestApi4XXErrorsAlarm(withId)(withAlarmId)(stack)(context)(input)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create an alarm for 4XX errors and override props', () => {

        const withId = (id: string) => id
        const withAlarmId = (alarmSource: string, alarmName: string) => `${alarmSource}-${alarmName}`

        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })

        const input: RestApiErrorAlarmInput = {
            id: 'Cocoa-Rest-Api-Test',
            alarmType: AlarmType.CRITICAL,
            apiId: 'jiygflgxzg',
            alarmDescription: 'Alarm description for Rest Api 4XX Errors',
            alarmThreshold: 100,
            alarmEvaluationPeriods: 1,
            alarmDatapoints: 1,
            alarmProps: {
                treatMissingData: TreatMissingData.IGNORE
            }
        }

        withRestApi4XXErrorsAlarm(withId)(withAlarmId)(stack)(context)(input)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

})

describe('withFunctionAlarms', () => {

    it('should create an alarm for Http (5XX) errors', () => {

        const withId = (id: string) => id
        const withAlarmId = (alarmSource: string, alarmName: string) => `${alarmSource}-${alarmName}`

        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })

        const input: FunctionErrorAlarmInput = {
            projectName: 'TestWrapper',
            functionName: 'TestFunction',
            alarmType: AlarmType.CRITICAL,
            alarmThreshold: 1,
            alarmEvaluationPeriods: 1,
            alarmDatapoints: 1
        }

        withFunctionHttpRequestErrorAlarm(withId)(withAlarmId)(stack)(context)('testNamespace')(input)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create an alarm for Http (5XX) errors and override props', () => {

        const withId = (id: string) => id
        const withAlarmId = (alarmSource: string, alarmName: string) => `${alarmSource}-${alarmName}`

        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })

        const input: FunctionErrorAlarmInput = {
            projectName: 'TestWrapper',
            functionName: 'TestFunction',
            alarmType: AlarmType.CRITICAL,
            alarmThreshold: 1,
            alarmEvaluationPeriods: 1,
            alarmDatapoints: 1,
            alarmProps: {
                treatMissingData: TreatMissingData.IGNORE
            },
            metricDuration: 2
        }

        withFunctionHttpRequestErrorAlarm(withId)(withAlarmId)(stack)(context)('testNamespace')(input)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create an alarm for count errors', () => {

        const withId = (id: string) => id
        const withAlarmId = (alarmSource: string, alarmName: string) => `${alarmSource}-${alarmName}`

        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })

        const input: FunctionErrorAlarmInput = {
            projectName: 'TestWrapper',
            functionName: 'TestFunction',
            alarmType: AlarmType.CRITICAL,
            alarmThreshold: 20,
            alarmEvaluationPeriods: 1,
            alarmDatapoints: 1
        }

        withFunctionCountAlarm(withId)(withAlarmId)(stack)(context)('testNamespace')(input)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create an alarm for count errors and override props', () => {

        const withId = (id: string) => id
        const withAlarmId = (alarmSource: string, alarmName: string) => `${alarmSource}-${alarmName}`

        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const context = withAlarmsContext(withId)(app, stack, 'TestProject', {
            criticalAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:CriticalAlarmSnsArn',
            mildAlarmTopicArn: 'arn:aws:sns:us-west-2:12345678890:MildAlarmSnsArn'
        })

        const input: FunctionErrorAlarmInput = {
            projectName: 'TestWrapper',
            functionName: 'TestFunction',
            alarmType: AlarmType.CRITICAL,
            alarmThreshold: 20,
            alarmEvaluationPeriods: 1,
            alarmDatapoints: 1,
            alarmProps: {
                treatMissingData: TreatMissingData.IGNORE
            }
        }

        withFunctionCountAlarm(withId)(withAlarmId)(stack)(context)('testNamespace')(input)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

})
