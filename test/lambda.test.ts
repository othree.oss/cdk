import {Template} from 'aws-cdk-lib/assertions'
import {Stack, App, Duration} from 'aws-cdk-lib/core'
import {lambda} from '../src'
import {Architecture, Code, Runtime, Tracing} from 'aws-cdk-lib/aws-lambda'
import {StackAccount} from '../src/core'
import {ServicePrincipal} from 'aws-cdk-lib/aws-iam'
import {Empty, Optional} from '@othree.io/optional'
import {DefaultStackSynthesizer} from 'aws-cdk-lib'

describe('lambda', () => {
    it('should create a lambda', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const withConcurrencyConfiguration = jest.fn().mockReturnValue(Empty())
        const withLambdaConfiguration = jest.fn().mockReturnValue(lambda.withDefaultConfiguration(app))
        lambda.withLambda(stack)(withConcurrencyConfiguration)(withLambdaConfiguration)({
            functionName: 'GetDataService-Test-Name',
            handler: 'index.handler',
            code: Code.fromInline(`
                    exports.handler = async (event) => {
                      console.log('Getting data: ', event)
                    };
                  `)
        }, {
            environment: {
                CONFIGURATION_INPUT: 'ConfigurationInput'
            }
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a lambda with concurrency configuration', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const withScalingConfiguration = jest.fn().mockReturnValue(Optional({
                minCapacity: 1,
                maxCapacity: 20,
                utilizationTarget: 0.5
            }))
        const withLambdaConfiguration = jest.fn().mockReturnValue(lambda.withDefaultConfiguration(app))
        lambda.withLambda(stack)(withScalingConfiguration)(withLambdaConfiguration)({
            functionName: 'GetDataService-Test-Name',
            handler: 'index.handler',
            code: Code.fromInline(`
                    exports.handler = async (event) => {
                      console.log('Getting data: ', event)
                    };
                  `)
        }, {
            environment: {
                CONFIGURATION_INPUT: 'ConfigurationInput'
            }
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})

describe('importFunction', () => {
    it('should reference an existing lambda function', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const fn = lambda.importFunction(stack)({arn: 'arn:aws:lambda:us-west-2:1337:function:FnName', id: 'TheId'})

        fn.grantInvoke(new ServicePrincipal('apigateway.amazonaws.com'))

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})

describe('functionArnFromName', () => {
    it('should return the correct ARN', () => {
        const stackAccount: StackAccount = {
            region: 'us-west-2',
            accountId: '1337'
        }
        const arn = lambda.functionArnFromName(stackAccount)('AwesomeFn')

        expect(arn).toEqual('arn:aws:lambda:us-west-2:1337:function:AwesomeFn')
    })

    it('should return the correct ARN with alias', () => {
        const stackAccount: StackAccount = {
            region: 'us-west-2',
            accountId: '1337'
        }
        const arn = lambda.functionArnFromName(stackAccount)('AwesomeFn', 'v1')

        expect(arn).toEqual('arn:aws:lambda:us-west-2:1337:function:AwesomeFn:v1')
    })
})

describe('functionArnFromNameAndDefaultAlias', () => {
    it('should return the correct ARN with the default alias', () => {
        const stackAccount: StackAccount = {
            region: 'us-west-2',
            accountId: '1337'
        }
        const arn = lambda.functionArnFromNameAndDefaultAlias(stackAccount)('AwesomeFn')

        expect(arn).toEqual('arn:aws:lambda:us-west-2:1337:function:AwesomeFn:LATEST')
    })
})

describe('withScalingConfiguration', () => {
    it('should return the concurrency configuration', () => {
        const app = new App({
            context: {
                lambda: {
                    'Cocoa-LambdaId-Dev': {
                        scaling: {
                            minCapacity: 1,
                            maxCapacity: 20,
                            utilizationTarget: 0.5
                        }
                    }
                }
            }
        })

        const maybeConfiguration = lambda.withScalingConfiguration(app)('Cocoa-LambdaId-Dev')

        expect(maybeConfiguration.isPresent).toBeTruthy()
        expect(maybeConfiguration.get()).toStrictEqual({
            minCapacity: 1,
            maxCapacity: 20,
            utilizationTarget: 0.5
        })
    })

    it('should return just the scaling configuration', () => {
        const app = new App({
            context: {
                lambda: {
                    'Cocoa-LambdaId-Dev': {
                        scaling: {
                            minCapacity: 1,
                            maxCapacity: 20,
                            utilizationTarget: 0.5
                        }
                    }
                }
            }
        })

        const maybeConfiguration = lambda.withScalingConfiguration(app)('Cocoa-LambdaId-Dev')

        expect(maybeConfiguration.isPresent).toBeTruthy()
        expect(maybeConfiguration.get()).toStrictEqual({
                minCapacity: 1,
                maxCapacity: 20,
                utilizationTarget: 0.5
            })
    })

    it('should return an empty optional if there is no lambda concurrency configuration', () => {
        const app = new App({
            context: {
                lambda: {
                    'Cocoa-LambdaId-Dev': {}
                }
            }
        })

        const maybeConfiguration = lambda.withScalingConfiguration(app)('Cocoa-LambdaId-Dev')

        expect(maybeConfiguration.isPresent).toBeFalsy()
        expect(maybeConfiguration.getError()).toBeUndefined()
    })

    it('should throw an error if scaling there is a misconfiguration', () => {
        const app = new App({
            context: {
                lambda: {
                    'Cocoa-LambdaId-Dev': {
                        scaling: {
                            minCapacity: 1
                        }
                    }
                }
            }
        })

        expect(() => lambda.withScalingConfiguration(app)('Cocoa-LambdaId-Dev')).toThrow(new Error('Incorrect lambda scaling configuration'))
    })
})

describe('importFunctionByPartialId', () => {
    it('should import a cocoa function', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const functionArn = jest.fn().mockReturnValue('arn:aws:lambda:us-west-2:1337:function:Cocoa-LambdaFunction-Dev')
        const withId = jest.fn().mockReturnValue('Cocoa-LambdaFunction-Dev')

        const fn = lambda.importFunctionByPartialId(functionArn)(withId)(lambda.importFunction(stack))('LambdaFunction')

        fn.grantInvoke(new ServicePrincipal('apigateway.amazonaws.com'))

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})

describe('withDefaultConfiguration', () => {
    it('should return the default configuration', () => {
        const app = new App()

        const configuration = lambda.withDefaultConfiguration(app)

        expect(configuration).toStrictEqual({
            timeout: Duration.seconds(30),
            tracing: Tracing.DISABLED,
            runtime: Runtime.NODEJS_18_X,
            memorySize: 256,
            architecture: Architecture.ARM_64
        })
    })

    it('should return the overridden default configuration', () => {
        const app = new App({
            context: {
                lambda: {
                    timeout: 60,
                    tracing: 'ACTIVE',
                    memorySize: 1024,
                    runtime: 'DOTNET_6',
                    architecture: 'X86_64'
                }
            }
        })

        const configuration = lambda.withDefaultConfiguration(app)

        expect(configuration).toStrictEqual({
            timeout: Duration.seconds(60),
            tracing: Tracing.ACTIVE,
            runtime: Runtime.DOTNET_6,
            memorySize: 1024,
            architecture: Architecture.X86_64
        })
    })
})

describe('withLambdaConfiguration', () => {
    it('should return the configured lambda configuration', () => {
        const app = new App({
            context: {
                lambda: {
                    'Cocoa-LambdaFunction-Dev': {
                        'runtime': 'DOTNET_6',
                        'tracing': 'ACTIVE',
                        'timeout': 60,
                        'memorySize': 1024,
                        'architecture': 'X86_64'
                    }
                }
            }
        })
        const configuration = lambda.withLambdaConfiguration(app)(lambda.withDefaultConfiguration)('Cocoa-LambdaFunction-Dev')

        expect(configuration).toStrictEqual({
            timeout: Duration.seconds(60),
            tracing: Tracing.ACTIVE,
            runtime: Runtime.DOTNET_6,
            memorySize: 1024,
            architecture: Architecture.X86_64,
            reservedConcurrentExecutions: undefined
        })
    })

    it('should return the default configuration if no configuration is specified for the lambda', () => {
        const app = new App()
        const configuration = lambda.withLambdaConfiguration(app)(lambda.withDefaultConfiguration)('Cocoa-LambdaFunction-Dev')

        expect(configuration).toStrictEqual({
            timeout: Duration.seconds(30),
            tracing: Tracing.DISABLED,
            runtime: Runtime.NODEJS_18_X,
            memorySize: 256,
            architecture: Architecture.ARM_64,
            reservedConcurrentExecutions: undefined
        })
    })

    it('should return the configured lambda configuration with the reserved concurrent executions', () => {
        const app = new App({
            context: {
                lambda: {
                    'Cocoa-LambdaFunction-Dev': {
                        'runtime': 'DOTNET_6',
                        'tracing': 'ACTIVE',
                        'timeout': 60,
                        'memorySize': 1024,
                        'architecture': 'X86_64',
                        'reservedConcurrentExecutions': 50
                    }
                }
            }
        })
        const configuration = lambda.withLambdaConfiguration(app)(lambda.withDefaultConfiguration)('Cocoa-LambdaFunction-Dev')

        expect(configuration).toStrictEqual({
            timeout: Duration.seconds(60),
            tracing: Tracing.ACTIVE,
            runtime: Runtime.DOTNET_6,
            memorySize: 1024,
            architecture: Architecture.X86_64,
            reservedConcurrentExecutions: 50
        })
    })
})
