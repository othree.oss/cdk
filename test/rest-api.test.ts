import {
    composeRestApi,
    HttpVerb,
    RestApiConfiguration,
    withDefaultApiConfiguration,
    withResource,
    withRestApi
} from '../src/rest-api'
import {Cors} from 'aws-cdk-lib/aws-apigateway'
import {App, DefaultStackSynthesizer, Stack} from 'aws-cdk-lib'
import {Template} from 'aws-cdk-lib/assertions'
import {Code, Function, Runtime} from 'aws-cdk-lib/aws-lambda'

describe('withDefaultApiConfiguration', () => {
    it('should return the default configuration', () => {
        expect(withDefaultApiConfiguration()).toMatchSnapshot()
    })
})

describe('withNewRestApi', () => {
    const configuration: RestApiConfiguration = {
        defaultCorsPreflightOptions: {
            allowHeaders: Cors.DEFAULT_HEADERS,
            allowMethods: ['GET'],
            allowOrigins: Cors.ALL_ORIGINS
        },
        usagePlanId: 'u54g3p14n'
    }
    it('should create a rest api with the specified resources', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const testFn1 = new Function(stack, 'TestFn1', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index',
            runtime: Runtime.NODEJS_14_X
        })

        const testFn2 = new Function(stack, 'TestFn2', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index2',
            runtime: Runtime.NODEJS_14_X
        })

        const api = withRestApi(configuration)(stack)({apiName: 'SuperApi'})

        composeRestApi(api,
            withResource('v1/orders/{orderId}/another/resource', {verb: HttpVerb.GET, fn: testFn1}),
            withResource('v1/orders/{orderId}/items', {
                verb: HttpVerb.PUT,
                fn: testFn2,
                props: {
                    apiKeyRequired: true
                }
            })
        )

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a rest api with the specified resources and a root method mapping', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const testFnRoot = new Function(stack, 'TestFnRoot', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index',
            runtime: Runtime.NODEJS_LATEST
        })

        const testFn1 = new Function(stack, 'TestFn1', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index',
            runtime: Runtime.NODEJS_LATEST
        })

        const testFn2 = new Function(stack, 'TestFn2', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index2',
            runtime: Runtime.NODEJS_LATEST
        })

        const api = withRestApi(configuration)(stack)({apiName: 'SuperApi'})

        composeRestApi(api,
            withResource('/', {verb: HttpVerb.GET, fn: testFnRoot}),
            withResource('/v1/orders/{orderId}/another/resource', {verb: HttpVerb.GET, fn: testFn1}),
            withResource('/v1/orders/{orderId}/items', {
                verb: HttpVerb.PUT,
                fn: testFn2,
                props: {
                    apiKeyRequired: true
                }
            })
        )

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a rest api with the specified subdomain and resources', () => {
        const withDomainConfiguration: RestApiConfiguration = {
            ...configuration,
            domain: {
                domain: 'othree.dev',
                basePath: 'test'
            }
        }
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const testFn1 = new Function(stack, 'TestFn1', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index',
            runtime: Runtime.NODEJS_LATEST
        })

        const testFn2 = new Function(stack, 'TestFn2', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index2',
            runtime: Runtime.NODEJS_LATEST
        })

        const api = withRestApi(withDomainConfiguration)(stack)({apiName: 'SuperApi'})

        composeRestApi(api,
            withResource('v1/orders/{orderId}/another/resource', {verb: HttpVerb.GET, fn: testFn1}),
            withResource('v1/orders/{orderId}/items', {
                verb: HttpVerb.PUT,
                fn: testFn2,
                props: {
                    apiKeyRequired: true
                }
            })
        )

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a rest api with api key', () => {
        const withDomainConfiguration: RestApiConfiguration = {
            ...withDefaultApiConfiguration(),
            domain: {
                domain: 'othree.dev',
                basePath: 'test'
            }
        }
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const testFn1 = new Function(stack, 'TestFn1', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index',
            runtime: Runtime.NODEJS_LATEST
        })

        const testFn2 = new Function(stack, 'TestFn2', {
            code: Code.fromAsset('./test/mock'),
            handler: 'index2',
            runtime: Runtime.NODEJS_LATEST
        })

        const api = withRestApi(withDomainConfiguration)(stack)({apiName: 'SuperApi'})

        composeRestApi(api,
            withResource('v1/orders/{orderId}/another/resource', {verb: HttpVerb.GET, fn: testFn1}),
            withResource('v1/orders/{orderId}/items', {
                verb: HttpVerb.PUT,
                fn: testFn2
            })
        )

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})
