import {withLocalAssetsVersionedBucketDeployment, withLocalAssetsBucketDeployment, withBucket} from '../src/s3'
import {Bucket} from 'aws-cdk-lib/aws-s3'
import {App, Stack} from 'aws-cdk-lib/core'
import {Template} from 'aws-cdk-lib/assertions'
import {DefaultStackSynthesizer} from 'aws-cdk-lib'

describe('s3', () => {

    it('should create a bucket', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const withId = jest.fn().mockReturnValue('Cocoa-TestBucket-Dev')

        withBucket(stack)(withId)({
            name: 'TestBucket'
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a local asset deployment', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const bucket = new Bucket(stack, 'TestBucket', {
            bucketName: 'testbucket'
        })

        const withId = jest.fn().mockReturnValue('Cocoa-LocalDeployment-Dev')

        withLocalAssetsBucketDeployment(stack)(withId)({
            name: 'TestDeployment',
            assetsPath: 'test/mock',
            bucket: bucket
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a local asset deployment and return the files hash', async () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const bucket = new Bucket(stack, 'TestBucket')

        const readdir = jest.fn().mockResolvedValue(['folder', 'file2'])
        const stat = jest.fn().mockImplementation((dir: string) => {
            if (dir === 'test/mock/folder') {
                return Promise.resolve({
                    isFile: () => false
                })
            } else if (dir === 'test/mock/file2') {
                return Promise.resolve({
                    isFile: () => true
                })
            } else {
                throw new Error('Unexpected dir')
            }
        })
        const readFile = jest.fn().mockResolvedValue(Buffer.from('File contents!'))
        const withLocalAssetsBucketDeployment = jest.fn().mockReturnValue(true)

        const update = jest.fn().mockImplementation((buffer: Buffer) => {
            const bufferStr = buffer.toString('utf-8')
            if (bufferStr === 'File contents!') {
                expect(bufferStr).toEqual('File contents!')
                return {
                    digest: () => 'C0nt3nts'
                }
            } else if (bufferStr === 'folder') {
                expect(bufferStr).toEqual('folder')
                return {
                    digest: () => 'f0ld3r'
                }
            } else {
                expect(bufferStr).toEqual('f0ld3r,C0nt3nts')
                return {
                    digest: () => 'f0ld3rC0nt3nts'
                }
            }

        })

        const createHash = jest.fn().mockImplementation((algo) => {
            expect(algo).toEqual('md5')
            return {
                update: update
            }
        })

        const generatedHash = await withLocalAssetsVersionedBucketDeployment(readdir)(stat)(readFile)(createHash)(withLocalAssetsBucketDeployment)({
            name: 'TestDeployment',
            assetsPath: 'test/mock',
            bucket: bucket
        })

        expect(generatedHash).toEqual({
            deploymentHash: 'f0ld3rC0nt3nts'
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})
