import {core} from '../src'
import {AppContext, getStackAccountFromEnv, IdInput, withCocoaIdInput} from '../src/core'
import {App} from 'aws-cdk-lib'

describe('withCocoaIdInput', () => {
    it('should return the cocoa id input', () => {
        const context: AppContext = {
            env: 'Dev',
            volatile: true,
            optimize: false,
            shareCommonDataStores: false
        }

        const input = withCocoaIdInput(context)

        expect(input).toStrictEqual({
            prefix: 'Cocoa',
            suffix: 'Dev'
        })
    })
})

describe('id', () => {
    it('should return the identifier', () => {
        const input: IdInput = {
            prefix: 'Cocoa',
            suffix: 'Dev'
        }
        const identifier = core.id('V1')(input)('TestProject-Lambda')

        expect(identifier).toEqual('Cocoa-TestProject-Lambda-Dev-V1')
    })

    it('should return the identifier with no prefix, suffix or version', () => {
        const identifier = core.id()({})('TestProject-Lambda')

        expect(identifier).toEqual('TestProject-Lambda')
    })

    it('should try to return the identifier and throw an error if the id is longer than 65 characters', () => {
        const input: IdInput = {
            prefix: 'Cocoa',
            suffix: 'Dev'
        }

        expect(() => core.id('v1')(input)('TestProject-Lambda-That-Is-Longer-Than-65-Characters-And-Should-Not-Work'))
            .toThrow('Id should be 65 characters or less')
    })
})

describe('appContext', () => {
    it('should return a AppContext from the provided app', () => {
        const cdkApp = new App({
            context: {
                env: 'Dev',
                volatile: 'true',
                optimize: false,
                shareCommonDataStores: false
            }
        })

        const context = core.appContext(cdkApp)

        expect(context).toStrictEqual({
            env: 'Dev',
            volatile: true,
            optimize: false,
            shareCommonDataStores: false
        })
    })
})

describe('getStackAccountFromEnv', () => {
    process.env.CDK_DEFAULT_ACCOUNT = '1234567890'
    process.env.CDK_DEFAULT_REGION = 'us-west-2'
    it('should return the configured stack', () => {
        const stackAccount = getStackAccountFromEnv()

        expect(stackAccount).toMatchSnapshot()
    })
})
