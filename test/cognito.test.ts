import {cognito, core} from '../src'
import {App, DefaultStackSynthesizer, Stack} from 'aws-cdk-lib'
import {CognitoAuth} from '../src/cognito'
import {Template} from 'aws-cdk-lib/assertions'

describe('cognito', () => {

    it('should create an identity pool role', () => {

        const app = new App({
            context: {
                env: 'Dev',
                volatile: 'true'
            }
        })

        const context = core.appContext(app)
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const withId = core.id('v1')(core.withCocoaIdInput(context))

        cognito.withCognitoRole(stack)(withId)('test-role',
            'us-west-2:1c564e71-94a8-4973-8ae9-7e48b84f703d',
            CognitoAuth.AUTHENTICATED)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create an identity pool role with prefix', () => {

        const app = new App({
            context: {
                env: 'Dev',
                volatile: 'true'
            }
        })

        const context = core.appContext(app)
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const withId = core.id('v1')(core.withCocoaIdInput(context))

        cognito.withCognitoRole(stack)(withId)('test-role',
            'us-west-2:1c564e71-94a8-4973-8ae9-7e48b84f703d',
            CognitoAuth.AUTHENTICATED,
            'RolePrefix')

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create an identity pool', () => {

        const app = new App({
            context: {
                env: 'Dev',
                volatile: 'true'
            }
        })

        const context = core.appContext(app)
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const withId = core.id('v1')(core.withCocoaIdInput(context))

        const withCognitoRole = jest.fn().mockReturnValue('arn:test-role')

        const input = {
            projectName: 'test-okta',
            userPoolId: 'us-west-2_JQ9m2Ww7n'
        }

        cognito.withIdentityPool(stack)(withId)(withCognitoRole)(input)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()

    })

    it('should create an okta cognito configuration', () => {

        const app = new App({
            context: {
                env: 'Dev',
                volatile: 'true'
            }
        })

        const context = core.appContext(app)
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const withId = core.id('v1')(core.withCocoaIdInput(context))

        const withCognitoRole = cognito.withCognitoRole(stack)(withId)
        const withIdentityPool = cognito.withIdentityPool(stack)(withId)(withCognitoRole)

        const input = {
            projectName: 'test-cognito-okta',
            userPoolId: 'us-west-2_JQ9m2Ww7n'
        }

        cognito.withOktaConfiguration(withIdentityPool)(input)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

})
