import {ec2} from '../src'
import {App, DefaultStackSynthesizer, Stack} from 'aws-cdk-lib'
import {Template} from 'aws-cdk-lib/assertions'
import {
    InstanceClass,
    InstanceSize,
    InstanceType,
    MachineImage,
    SubnetType,
    Vpc,
    WindowsVersion
} from 'aws-cdk-lib/aws-ec2'

describe('ec2', () => {
    it('should crate an ec2 instance', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const withId = jest.fn().mockImplementation((id: string) => `Test-${id}-V1`)

        const withDefaultEc2InstanceRole = ec2.withDefaultEc2InstanceRole(stack)
        const withKeyPair = ec2.withKeyPair(stack)

        const vpc = new Vpc(stack, 'Vpc', {
            maxAzs: 1,
            subnetConfiguration: [{
                cidrMask: 28,
                name: 'ingress',
                subnetType: SubnetType.PUBLIC,
            }],
            cidr: '10.0.0.0/24'
        })

        const defaultRdpSecurityGroup = ec2.withDefaultRdpSecurityGroup(stack)('Test-SecurityGroup', vpc)

        ec2.withEc2Instance(stack)(withId)(withDefaultEc2InstanceRole)(withKeyPair)({
            name: 'Test-Instance',
            vpc: vpc,
            subnetSelection: vpc.selectSubnets({
                subnetType: SubnetType.PUBLIC
            }),
            instanceType: InstanceType.of(InstanceClass.T3, InstanceSize.MEDIUM),
            image: MachineImage.latestWindows(WindowsVersion.WINDOWS_SERVER_2019_ENGLISH_FULL_BASE),
            securityGroup: defaultRdpSecurityGroup
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create the default instance role', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        ec2.withDefaultEc2InstanceRole(stack)('Test-Instance-Role')

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create the default rdp security group', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        const vpc = new Vpc(stack, 'Vpc', {
            maxAzs: 1,
            subnetConfiguration: [{
                cidrMask: 28,
                name: 'ingress',
                subnetType: SubnetType.PUBLIC,
            }],
            cidr: '10.0.0.0/24'
        })

        ec2.withDefaultRdpSecurityGroup(stack)('Test-Instance-Role', vpc)

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })

    it('should create a key pair', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })

        ec2.withKeyPair(stack)('Test-Instance-Key')

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})
