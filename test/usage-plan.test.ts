import {addApiStageToUsagePlan} from '../src/usage-plan'
import {App, Stack} from 'aws-cdk-lib/core'
import {DefaultStackSynthesizer} from 'aws-cdk-lib'
import {Template} from 'aws-cdk-lib/assertions'


describe('AddApiStageToUsagePlan', () => {
    it('', () => {
        const app = new App()
        const stack = new Stack(app, 'TestStack', {
            synthesizer: new DefaultStackSynthesizer({
                generateBootstrapVersionRule: false
            })
        })
        const usagePlan = addApiStageToUsagePlan(stack, 'test', {
            ApiId: '4p1',
            Stage: '5t4g3',
            UsagePlanId: 'p14n'
        })

        const template = Template.fromStack(stack)
        expect(template.toJSON()).toMatchSnapshot()
    })
})
